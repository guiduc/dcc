# DCC Control System

## Introduction

A more detailed description of the project is available here: https://www.guiduc.org/projects/dcc/

## License

Copyright 2020 Guillaume Duc <guillaume@guiduc.org>

All source code (firmware...) is licensed under either of

 * Apache License, Version 2.0, ([LICENSE-APACHE](LICENSE-APACHE) or http://www.apache.org/licenses/LICENSE-2.0)
 * MIT license ([LICENSE-MIT](LICENSE-MIT) or http://opensource.org/licenses/MIT)

at your option.

The hardware parts (KiCad projects: schematics, PCB...) are licensed
under the CERN-OHL-Pv2
([LICENSE-CERN-OHL-P-V2](LICENSE-CERN-OHL-P-V2)).

### Contribution

Unless you explicitly state otherwise, any contribution intentionally
submitted for inclusion in the work by you, as defined in the Apache-2.0
license, shall be dual licensed as above, without any additional terms
or conditions.
