# Remote control hardware project

This directory contains the [KiCad](https://kicad-pcb.org/) project
for the DCC remote control.

## License

Copyright 2020 Guillaume Duc <guillaume@guiduc.org>

This project is licensed under the CERN-OHL-Pv2
([LICENSE-CERN-OHL-P-V2](../../LICENSE-CERN-OHL-P-V2)).
