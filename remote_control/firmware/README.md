# Remote control firmware

## Roadmap

### 0.2

- Basic menu (DCC address, fonctions for keys4..6)

### 0.3

- Load/Save config from/to EEPROM

## License

Copyright 2020 Guillaume Duc <guillaume@guiduc.org>

Licensed under either of

 * Apache License, Version 2.0, ([LICENSE-APACHE](../../LICENSE-APACHE) or http://www.apache.org/licenses/LICENSE-2.0)
 * MIT license ([LICENSE-MIT](../../LICENSE-MIT) or http://opensource.org/licenses/MIT)

at your option.
