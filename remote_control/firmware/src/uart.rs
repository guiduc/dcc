use crate::hal::{
    prelude::*,
    rcc::Clocks,
    serial::{config, NoRx, Serial},
    stm32,
};

pub type Uart = Serial<stm32::USART1, (super::board::UartTxPin, NoRx)>;

pub fn uart_init(uart: stm32::USART1, tx_pin: super::board::UartTxPin, clocks: &Clocks) -> Uart {
    let usart1_config = config::Config {
        baudrate: 9_600.bps(),
        wordlength: config::WordLength::DataBits8,
        parity: config::Parity::ParityNone,
        stopbits: config::StopBits::STOP1,
    };

    let usart1 = Serial::usart1(uart, (tx_pin, NoRx), usart1_config, *clocks).unwrap();

    usart1
}
