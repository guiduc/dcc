use crate::hal::{block, prelude::*};

use arrayvec::ArrayString;

pub struct Screen {
    pub first_line: ArrayString<[u8; 16]>,
    pub second_line: ArrayString<[u8; 16]>,
}

impl Default for Screen {
    fn default() -> Self {
        Screen {
            first_line: ArrayString::<[u8; 16]>::new(),
            second_line: ArrayString::<[u8; 16]>::new(),
        }
    }
}

impl Screen {
    pub fn write(&self, uart: &mut super::uart::Uart) {
        // Set position = line 0, col 0
        block!(uart.write(0xfe)).unwrap();
        block!(uart.write(0x45)).unwrap();
        block!(uart.write(0x00)).unwrap();

        for b in self.first_line.bytes() {
            block!(uart.write(b)).unwrap();
        }
        for _i in 0..(16 - self.first_line.len()) {
            block!(uart.write(b' ')).unwrap();
        }

        // Set position = line 1, col 0
        block!(uart.write(0xfe)).unwrap();
        block!(uart.write(0x45)).unwrap();
        block!(uart.write(0x40)).unwrap();

        for b in self.second_line.bytes() {
            block!(uart.write(b)).unwrap();
        }
        for _i in 0..(16 - self.second_line.len()) {
            block!(uart.write(b' ')).unwrap();
        }
    }
}

pub fn clear_screen(uart: &mut super::uart::Uart) {
    block!(uart.write(0xfe)).unwrap();
    block!(uart.write(0x51)).unwrap();
}
