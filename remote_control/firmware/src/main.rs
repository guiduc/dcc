#![no_std]
#![no_main]

extern crate panic_halt;

mod board;
mod can;
mod screen;
mod state;
mod uart;

use stm32f4xx_hal as hal;

use crate::hal::{prelude::*, stm32, timer::Event, timer::Timer};

const VERSION_MAJOR: u8 = 0;
const VERSION_MINOR: u8 = 1;

type ButtonTimer = Timer<stm32::TIM2>;
type ScreenTimer = Timer<stm32::TIM3>;

#[rtic::app(device = stm32f4xx_hal::stm32, peripherals = true)]
const APP: () = {
    struct Resources {
        state: state::State,
        uart: uart::Uart,
        can: can::Can,
        keys: board::Keys,
        button_timer: ButtonTimer,
        screen_timer: ScreenTimer,
        encoder_timer: board::EncoderTimer,
    }

    #[init]
    fn init(cx: init::Context) -> init::LateResources {
        // Cortex-M peripherals
        //let _core = cx.core;

        // Device specific peripherals
        let dp: stm32::Peripherals = cx.device;

        // Clocks init
        let rcc = dp.RCC.constrain();
        let clocks = board::setup_clocks(rcc);

        // GPIO init
        let gpioa = dp.GPIOA.split();
        let gpiob = dp.GPIOB.split();
        let gpioc = dp.GPIOC.split();

        let pins = board::pin_init(gpioa, gpiob, gpioc);
        let keys = pins.keys;

        // Encoder timer
        let encoder_timer = dp.TIM4;
        board::encoder_init(&encoder_timer);

        // USART
        let mut uart = uart::uart_init(dp.USART1, pins.uart_tx_pin, &clocks);

        // CAN
        let can = dp.CAN1;
        can::can_init(&can, pins.can_stby_pin);

        // State init
        let state: state::State = Default::default();
        //state.speed = Some(0);

        screen::clear_screen(&mut uart);

        // Buttons timer
        let mut button_timer = Timer::tim2(dp.TIM2, 20.hz(), clocks);
        button_timer.listen(Event::TimeOut);
        stm32::NVIC::unpend(hal::stm32::Interrupt::TIM2);
        unsafe {
            stm32::NVIC::unmask(hal::stm32::Interrupt::TIM2);
        }

        // Display timer
        let mut screen_timer = Timer::tim3(dp.TIM3, 5.hz(), clocks);
        screen_timer.listen(Event::TimeOut);
        stm32::NVIC::unpend(hal::stm32::Interrupt::TIM3);
        unsafe {
            stm32::NVIC::unmask(hal::stm32::Interrupt::TIM3);
        }

        init::LateResources {
            state,
            uart,
            can,
            keys,
            button_timer,
            screen_timer,
            encoder_timer,
        }
    }

    #[task(binds = TIM3, resources = [state, screen_timer, uart])]
    fn display_screen_task(cx: display_screen_task::Context) {
        let state: &mut state::State = cx.resources.state;
        let screen_timer: &mut ScreenTimer = cx.resources.screen_timer;
        let uart: &mut uart::Uart = cx.resources.uart;

        screen_timer.clear_interrupt(Event::TimeOut);

        let screen = state.format();
        screen.write(uart);
    }

    #[task(binds = TIM2, resources = [state, keys, button_timer, encoder_timer, can])]
    fn button_task(cx: button_task::Context) {
        static mut KEYS_STATE_OLD: board::KeysState = board::KeysState {
            key1: true,
            key2: true,
            key3: true,
            key4: true,
            key5: true,
            key6: true,
            key_re: true,
            encoder: 0,
        };

        let state: &mut state::State = cx.resources.state;
        let button_timer: &mut ButtonTimer = cx.resources.button_timer;
        let encoder_timer: &mut board::EncoderTimer = cx.resources.encoder_timer;
        let keys: &mut board::Keys = cx.resources.keys;
        let can: &mut can::Can = cx.resources.can;

        button_timer.clear_interrupt(Event::TimeOut);

        let events = board::read_buttons(encoder_timer, keys, KEYS_STATE_OLD);
        state.update_state(events);
        state.output_messages.iter().for_each(|m| m.send(can));
        state.output_messages.clear();
    }

    #[task(binds = CAN1_RX0, resources = [state, can])]
    fn can_rx_event(cx: can_rx_event::Context) {
        let state: &mut state::State = cx.resources.state;
        let can: &mut can::Can = cx.resources.can;

        while let Some(message) = can::receive_can_message(can) {
            state.process_message(message);
        }
    }
};
