// Pins
// PA9 : USART1_TX (AF7)

use crate::hal::{
    gpio::{gpioa, gpiob, gpioc, Alternate, Input, Output, PullUp, PushPull, AF7, AF9},
    prelude::*,
    rcc::{Clocks, Rcc},
    stm32,
};

pub type UartTxPin = gpioa::PA9<Alternate<AF7>>;
pub type Key1Pin = gpiob::PB10<Input<PullUp>>;
pub type Key2Pin = gpioc::PC3<Input<PullUp>>;
pub type Key3Pin = gpiob::PB1<Input<PullUp>>;
pub type Key4Pin = gpiob::PB13<Input<PullUp>>;
pub type Key5Pin = gpioa::PA2<Input<PullUp>>;
pub type Key6Pin = gpiob::PB5<Input<PullUp>>;
pub type KeyRePin = gpiob::PB11<Input<PullUp>>;

pub type CanStbyPin = gpioc::PC0<Output<PushPull>>;
pub type CanRxPin = gpiob::PB8<Alternate<AF9>>;
pub type CanTxPin = gpiob::PB9<Alternate<AF9>>;

pub type EncoderTimer = stm32::TIM4;

pub struct Keys {
    pub key1_pin: Key1Pin,
    pub key2_pin: Key2Pin,
    pub key3_pin: Key3Pin,
    pub key4_pin: Key4Pin,
    pub key5_pin: Key5Pin,
    pub key6_pin: Key6Pin,
    pub key_re_pin: KeyRePin,
}

pub struct KeysState {
    pub key1: bool,
    pub key2: bool,
    pub key3: bool,
    pub key4: bool,
    pub key5: bool,
    pub key6: bool,
    pub key_re: bool,
    pub encoder: u32,
}

#[derive(Default)]
pub struct KeysEvent {
    pub key1_pushed: bool,
    pub key2_pushed: bool,
    pub key3_pushed: bool,
    pub key4_pushed: bool,
    pub key5_pushed: bool,
    pub key6_pushed: bool,
    pub key_re_pushed: bool,
    pub re_delta: i32,
}

pub struct Pins {
    pub keys: Keys,
    pub uart_tx_pin: UartTxPin,
    pub can_stby_pin: CanStbyPin,
    pub can_rx_pin: CanRxPin,
    pub can_tx_pin: CanTxPin,
}

pub fn setup_clocks(rcc: Rcc) -> Clocks {
    rcc.cfgr
        .use_hse(8.mhz())
        .sysclk(168.mhz())
        .pclk1(42.mhz())
        .pclk2(84.mhz())
        .freeze()
}

pub fn pin_init(gpioa: gpioa::Parts, gpiob: gpiob::Parts, gpioc: gpioc::Parts) -> Pins {
    // GPIO
    let key1_pin = gpiob.pb10.into_pull_up_input();
    let key2_pin = gpioc.pc3.into_pull_up_input();
    let key3_pin = gpiob.pb1.into_pull_up_input();
    let key4_pin = gpiob.pb13.into_pull_up_input();
    let key5_pin = gpioa.pa2.into_pull_up_input();
    let key6_pin = gpiob.pb5.into_pull_up_input();
    let key_re_pin = gpiob.pb11.into_pull_up_input();

    let re_a_pin = gpiob.pb6.into_alternate_af2();
    re_a_pin.internal_pull_up(true);
    let re_b_pin = gpiob.pb7.into_alternate_af2();
    re_b_pin.internal_pull_up(true);

    // UART
    let uart_tx_pin = gpioa.pa9.into_alternate_af7();

    // SPI
    let _spi_nss = gpioa.pa4.into_alternate_af5();
    let _spi_sck = gpioa.pa5.into_alternate_af5();
    let _spi_miso = gpioa.pa6.into_alternate_af5();
    let _spi_mosi = gpioa.pa7.into_alternate_af5();

    // CAN
    let mut can_stby_pin = gpioc.pc0.into_push_pull_output();
    can_stby_pin.set_high().unwrap();
    let can_rx_pin = gpiob.pb8.into_alternate_af9();
    let can_tx_pin = gpiob.pb9.into_alternate_af9();

    Pins {
        keys: Keys {
            key1_pin,
            key2_pin,
            key3_pin,
            key4_pin,
            key5_pin,
            key6_pin,
            key_re_pin,
        },
        uart_tx_pin,
        can_stby_pin,
        can_rx_pin,
        can_tx_pin,
    }
}

pub fn encoder_init(tim: &EncoderTimer) {
    // Enable TIM4 clock & reset
    let rcc = unsafe { &(*stm32::RCC::ptr()) };
    rcc.apb1enr.modify(|_, w| w.tim4en().set_bit());
    rcc.apb1rstr.modify(|_, w| w.tim4rst().set_bit());
    rcc.apb1rstr.modify(|_, w| w.tim4rst().clear_bit());

    // Encoder mode 1
    tim.smcr.write(|w| w.sms().bits(0b001));
    // IC2 = TI2, IC1 = TI1
    tim.ccmr1_input_mut()
        .write(|w| unsafe { w.cc2s().bits(0b01).cc1s().bits(0b01) });
    // Enable counter
    tim.cr1.modify(|_, w| w.cen().set_bit());
}

pub fn read_buttons(
    encoder_timer: &mut super::board::EncoderTimer,
    keys: &mut Keys,
    keys_state_old: &mut KeysState,
) -> KeysEvent {
    let mut events: KeysEvent = Default::default();

    let key1_new = keys.key1_pin.is_high().unwrap();
    if keys_state_old.key1 != key1_new {
        if !key1_new {
            events.key1_pushed = true;
        }
        keys_state_old.key1 = key1_new;
    }

    let key2_new = keys.key2_pin.is_high().unwrap();
    if keys_state_old.key2 != key2_new {
        if !key2_new {
            events.key2_pushed = true;
        }
        keys_state_old.key2 = key2_new;
    }

    let key3_new = keys.key3_pin.is_high().unwrap();
    if keys_state_old.key3 != key3_new {
        if !key3_new {
            events.key3_pushed = true;
        }
        keys_state_old.key3 = key3_new;
    }

    let key4_new = keys.key4_pin.is_high().unwrap();
    if keys_state_old.key4 != key4_new {
        if !key4_new {
            events.key4_pushed = true;
        }
        keys_state_old.key4 = key4_new;
    }

    let key5_new = keys.key5_pin.is_high().unwrap();
    if keys_state_old.key5 != key5_new {
        if !key5_new {
            events.key5_pushed = true;
        }
        keys_state_old.key5 = key5_new;
    }

    let key6_new = keys.key6_pin.is_high().unwrap();
    if keys_state_old.key6 != key6_new {
        if !key6_new {
            events.key6_pushed = true;
        }
        keys_state_old.key6 = key6_new;
    }

    let key_re_new = keys.key_re_pin.is_high().unwrap();
    if keys_state_old.key_re != key_re_new {
        if !key_re_new {
            events.key_re_pushed = true;
        }
        keys_state_old.key_re = key_re_new;
    }

    // Encoder computations
    let encoder = encoder_timer.cnt.read().bits() / 2; // Each step inc/dec the counter by 2

    let delta_pos: u32;
    let delta_neg: u32;
    if encoder > keys_state_old.encoder {
        delta_pos = encoder - keys_state_old.encoder;
        delta_neg = ((u16::MAX / 2) as u32) - delta_pos; // TIM4 counter is a 16 bit value
    } else if encoder < keys_state_old.encoder {
        delta_neg = keys_state_old.encoder - encoder;
        delta_pos = ((u16::MAX / 2) as u32) - delta_neg; // TIM4 counter is a 16 bit value
    } else {
        delta_neg = 0;
        delta_pos = 0;
    }
    if delta_pos < delta_neg {
        events.re_delta = delta_pos as i32;
    } else if delta_pos > delta_neg {
        events.re_delta = -(delta_neg as i32);
    }

    keys_state_old.encoder = encoder;

    events
}
