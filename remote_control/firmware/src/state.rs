use super::can::InputMessage;
use super::can::OutputMessage;
use arrayvec::ArrayVec;
use core::fmt;

#[derive(PartialEq)]
pub enum Mode {
    Init,
    Normal,
    Emergency,
    Menu,
}

pub struct State {
    pub current_mode: Mode,
    pub loco_address: u8,
    pub local_forward: Option<bool>,
    pub remote_forward: Option<bool>,
    pub local_speed: Option<u8>,
    pub remote_speed: Option<u8>,
    pub key4_fn: u8,
    pub key5_fn: u8,
    pub key6_fn: u8,
    pub output_messages: ArrayVec<[OutputMessage; 2]>,
}

impl Default for State {
    fn default() -> Self {
        State {
            current_mode: Mode::Init,
            loco_address: 3,
            local_forward: None,
            remote_forward: None,
            local_speed: None,
            remote_speed: None,
            key4_fn: 0,
            key5_fn: 1,
            key6_fn: 3, // TODO
            output_messages: ArrayVec::<[OutputMessage; 2]>::new(),
        }
    }
}

impl State {
    pub fn format(&self) -> super::screen::Screen {
        match &self.current_mode {
            Mode::Init => self.format_init(),
            Mode::Normal => self.format_normal(),
            Mode::Emergency => self.format_emergency(),
            Mode::Menu => self.format_menu(),
        }
    }

    fn format_init(&self) -> super::screen::Screen {
        let mut screen: super::screen::Screen = Default::default();

        screen.first_line.push_str("DCC Remote Ctrl");
        fmt::write(
            &mut screen.second_line,
            format_args!("GD v{}.{}", super::VERSION_MAJOR, super::VERSION_MINOR),
        )
        .unwrap();

        screen
    }

    fn format_normal(&self) -> super::screen::Screen {
        let mut screen: super::screen::Screen = Default::default();

        // First line
        fmt::write(
            &mut screen.first_line,
            format_args!("#{:03} ", self.loco_address),
        )
        .unwrap();

        match self.local_forward {
            Some(true) => screen.first_line.push_str("F"),
            Some(false) => screen.first_line.push_str("R"),
            None => screen.first_line.push_str("?"),
        }
        match self.local_speed {
            Some(speed) => {
                fmt::write(&mut screen.first_line, format_args!("{:02}", speed)).unwrap()
            }
            None => screen.first_line.push_str("--"),
        }
        match self.remote_forward {
            Some(true) => screen.first_line.push_str("/F"),
            Some(false) => screen.first_line.push_str("/R"),
            None => screen.first_line.push_str("/?"),
        }
        match self.remote_speed {
            Some(speed) => {
                fmt::write(&mut screen.first_line, format_args!("{:02}", speed)).unwrap()
            }
            None => screen.first_line.push_str("--"),
        }

        // Second line
        fmt::write(
            &mut screen.second_line,
            format_args!(
                "F{:02}  F{:02}  F{:02}",
                self.key4_fn, self.key5_fn, self.key6_fn
            ),
        )
        .unwrap();

        screen
    }

    fn format_emergency(&self) -> super::screen::Screen {
        let mut screen: super::screen::Screen = Default::default();

        screen.first_line.push_str("EMERGENCY STOP");

        screen
    }

    // TODO
    fn format_menu(&self) -> super::screen::Screen {
        let screen: super::screen::Screen = Default::default();
        screen
    }

    fn from_init_to_normal_state(&mut self) {
        self.current_mode = Mode::Normal;
        self.output_messages
            .push(OutputMessage::LocomotiveRequestStatus {
                address: self.loco_address,
            });
    }

    fn from_emergency_to_normal_state(&mut self) {
        self.current_mode = Mode::Normal;
        self.local_forward = None;
        self.local_speed = None;
        self.output_messages.push(OutputMessage::EnablePower);
        self.output_messages
            .push(OutputMessage::LocomotiveRequestStatus {
                address: self.loco_address,
            });
    }

    fn from_normal_to_emergency_state(&mut self) {
        self.current_mode = Mode::Emergency;
        self.output_messages.push(OutputMessage::EmergencyPowerDown);
    }

    fn key1_pressed(&mut self) {
        match self.current_mode {
            Mode::Init => self.from_init_to_normal_state(),
            Mode::Normal => self.from_normal_to_emergency_state(),
            _ => {}
        }
    }

    fn key2_pressed(&mut self) {
        match self.current_mode {
            Mode::Init => self.from_init_to_normal_state(),
            Mode::Emergency => self.from_emergency_to_normal_state(),
            _ => {}
        }
    }

    fn key3_pressed(&mut self) {
        match self.current_mode {
            Mode::Init => self.from_init_to_normal_state(),
            Mode::Normal => {
                if self.local_forward.is_some()
                    && self.local_speed.is_some()
                    && (self.local_speed.unwrap() == 0)
                {
                    self.local_forward = Some(!self.local_forward.unwrap());
                    self.output_messages.push(OutputMessage::LocomotiveSpeed {
                        address: self.loco_address,
                        forward: self.local_forward.unwrap(),
                        speed: self.local_speed.unwrap(),
                    });
                }
            }
            Mode::Emergency => self.from_emergency_to_normal_state(),
            _ => {}
        }
    }

    fn key4_pressed(&mut self) {
        match self.current_mode {
            Mode::Init => self.from_init_to_normal_state(),
            Mode::Normal => {
                self.output_messages
                    .push(OutputMessage::LocomotiveToggleFn {
                        address: self.loco_address,
                        fonction_num: self.key4_fn,
                    });
            }
            Mode::Emergency => self.from_emergency_to_normal_state(),
            _ => {}
        }
    }

    fn key5_pressed(&mut self) {
        match self.current_mode {
            Mode::Init => self.from_init_to_normal_state(),
            Mode::Normal => {
                self.output_messages
                    .push(OutputMessage::LocomotiveToggleFn {
                        address: self.loco_address,
                        fonction_num: self.key5_fn,
                    });
            }
            Mode::Emergency => self.from_emergency_to_normal_state(),
            _ => {}
        }
    }

    fn key6_pressed(&mut self) {
        match self.current_mode {
            Mode::Init => self.from_init_to_normal_state(),
            Mode::Normal => {
                self.output_messages
                    .push(OutputMessage::LocomotiveToggleFn {
                        address: self.loco_address,
                        fonction_num: self.key6_fn,
                    });
            }
            Mode::Emergency => self.from_emergency_to_normal_state(),
            _ => {}
        }
    }

    fn key_re_pressed(&mut self) {
        match self.current_mode {
            Mode::Init => self.from_init_to_normal_state(),
            Mode::Normal => {
                if self.local_speed.is_some() {
                    self.local_speed = Some(0);
                    self.output_messages.push(OutputMessage::LocomotiveStop {
                        address: self.loco_address,
                    });
                }
            }
            Mode::Emergency => self.from_emergency_to_normal_state(),
            _ => {}
        }
    }

    fn update_speed(&mut self, delta: i32) {
        if self.current_mode != Mode::Normal || self.local_speed.is_none() {
            return;
        }

        let new_speed: i32 = (self.local_speed.unwrap() as i32 + delta).max(0).min(28);

        self.local_speed = Some(new_speed as u8);

        self.output_messages.push(OutputMessage::LocomotiveSpeed {
            address: self.loco_address,
            forward: self.local_forward.unwrap(),
            speed: self.local_speed.unwrap(),
        });
    }

    pub fn update_state(&mut self, events: super::board::KeysEvent) {
        if events.key1_pushed {
            self.key1_pressed();
        }
        if events.key2_pushed {
            self.key2_pressed();
        }
        if events.key3_pushed {
            self.key3_pressed();
        }
        if events.key4_pushed {
            self.key4_pressed();
        }
        if events.key5_pushed {
            self.key5_pressed();
        }
        if events.key6_pushed {
            self.key6_pressed();
        }
        if events.key_re_pushed {
            self.key_re_pressed();
        }
        if events.re_delta != 0 {
            self.update_speed(events.re_delta);
        }
    }

    pub fn process_message(&mut self, message: InputMessage) {
        match message {
            InputMessage::LocomotiveStatus {
                address,
                forward,
                speed,
                f7_0,
                f15_8,
                f23_16,
            } => {
                if self.current_mode == Mode::Normal && self.loco_address == address {
                    self.remote_forward = Some(forward);
                    if self.local_forward.is_none() {
                        self.local_forward = Some(forward);
                    }
                    self.remote_speed = Some(speed);
                    if self.local_speed.is_none() {
                        self.local_speed = Some(speed);
                    }
                }
            }
        }
    }
}
