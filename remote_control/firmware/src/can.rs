use crate::hal::{prelude::*, stm32};
use stm32f4xx_hal as hal;

pub type Can = stm32::CAN1;

const MSG_GENERAL: u8 = 0b000;
const MSG_LOCOMOTIVE: u8 = 0b001;

const MSG_GENERAL_EMERGENCY: u8 = 0b0000;
const MSG_GENERAL_ENABLE_POWER: u8 = 0b0100;

const MSG_LOCO_ESTOP: u8 = 0b0000;
const MSG_LOCO_STOP: u8 = 0b0001;
const MSG_LOCO_SET_SPEED: u8 = 0b0010;
const MSG_LOCO_SET_FUNCTIONS: u8 = 0b0011;
const MSG_LOCO_UPDATE_SET_FUNCTIONS: u8 = 0b0100;
const MSG_LOCO_UPDATE_CLEAR_FUNCTIONS: u8 = 0b0101;
const MSG_LOCO_UPDATE_TOGGLE_FUNCTIONS: u8 = 0b0110;
const MSG_LOCO_STATUS: u8 = 0b1110;
const MSG_LOCO_REQ_STATUS: u8 = 0b1111;

const MSG_SENDER_REMOTE1: u8 = 0b0010;

struct CanRxMsg {
    stid: u16,
    exid: u32,
    ide: bool,
    dlc: u8,
    data: [u8; 8],
}

struct CanTxMsg {
    stid: u16,
    exid: u32,
    ide: bool,
    dlc: u8,
    data: [u8; 8],
}

pub enum OutputMessage {
    EmergencyPowerDown,
    EnablePower,
    LocomotiveSpeed {
        address: u8,
        forward: bool,
        speed: u8,
    },
    LocomotiveStop {
        address: u8,
    },
    LocomotiveRequestStatus {
        address: u8,
    },
    LocomotiveToggleFn {
        address: u8,
        fonction_num: u8,
    },
}

pub enum InputMessage {
    LocomotiveStatus {
        address: u8,
        forward: bool,
        speed: u8,
        f7_0: u8,
        f15_8: u8,
        f23_16: u8,
    },
}

pub fn can_init(can: &Can, mut can_stby_pin: super::board::CanStbyPin) {
    let rcc = unsafe { &(*stm32::RCC::ptr()) };

    // Enable CAN1 clock
    rcc.apb1enr.modify(|_, w| w.can1en().set_bit());

    // Reset CAN1
    rcc.apb1rstr.modify(|_, w| w.can1rst().set_bit());
    rcc.apb1rstr.modify(|_, w| w.can1rst().clear_bit());

    // Sleep -> Initialization mode
    can.mcr.write(|w| w.sleep().clear_bit().inrq().set_bit());
    while !can.msr.read().inak().bit() {}

    // Set the transceiver in Normal Mode
    can_stby_pin.set_low().unwrap();

    // The APB1 clock frequency is 42 MHz
    // t_q = (BRP+1)*t_pclk = (20+1)*(1/42e6) = 0.5 us
    // bit_time = t_q + t_bs2 + t_bs1 = t_q + t_q * (TS2+1) + t_q * (TS1+1) = t_q * (3+TS1+TS2) = t_q * (3+3+2) = 8*t_q = 4 us
    // bit_rate = 1 / 4us = 250 kbps
    can.btr
        .write(|w| unsafe { w.sjw().bits(1).ts2().bits(2).ts1().bits(3).brp().bits(20) });

    // Configure receive filters
    can.fmr
        .write(|w| unsafe { w.can2sb().bits(28).finit().set_bit() });

    // Filter general messages (STID[10:8] == 0b000)
    can.fm1r.write(|w| w.fbm0().clear_bit()); // Mask mode
    can.fs1r.write(|w| w.fsc0().set_bit()); // signle 32-bit configuration
    can.ffa1r.write(|w| w.ffa0().clear_bit()); // FIFO 0
                                               // STID[10:3], STID[2:0], EXID[17:13], EXID[12:5], EXID[4:0], IDE, RTR, 0
    can.fb[0]
        .fr1
        .write(|w| unsafe { w.fb().bits(0b0000_0000_0000_0000_0000_0000_0000_0000) }); // ID
    can.fb[0]
        .fr2
        .write(|w| unsafe { w.fb().bits(0b1110_0000_0000_0000_0000_0000_0000_0000) }); // Mask
    can.fa1r.write(|w| w.fact0().set_bit()); // Activate filter

    // Filter locomotive messages (STID[10:8] == 0b001)
    can.fm1r.modify(|_, w| w.fbm1().clear_bit()); // Mask mode
    can.fs1r.modify(|_, w| w.fsc1().set_bit()); // signle 32-bit configuration
    can.ffa1r.modify(|_, w| w.ffa1().clear_bit()); // FIFO 0
                                                   // STID[10:3], STID[2:0], EXID[17:13], EXID[12:5], EXID[4:0], IDE, RTR, 0
    can.fb[1]
        .fr1
        .write(|w| unsafe { w.fb().bits(0b0010_0000_0000_0000_0000_0000_0000_0000) }); // ID
    can.fb[1]
        .fr2
        .write(|w| unsafe { w.fb().bits(0b1110_0000_0000_0000_0000_0000_0000_0000) }); // Mask
    can.fa1r.modify(|_, w| w.fact1().set_bit()); // Activate filter

    can.fmr
        .write(|w| unsafe { w.can2sb().bits(28).finit().clear_bit() });

    // Enable RX0 interrupt
    can.ier.write(|w| w.fmpie0().set_bit());
    stm32::NVIC::unpend(hal::stm32::Interrupt::CAN1_RX0);

    unsafe {
        stm32::NVIC::unmask(hal::stm32::Interrupt::CAN1_RX0);
    }

    // Initialization -> Normal mode
    can.mcr
        .write(|w| w.sleep().clear_bit().inrq().clear_bit().abom().set_bit());
    while can.msr.read().inak().bit() {}
    while can.msr.read().slak().bit() {}
}

pub fn receive_can_message(can: &mut Can) -> Option<InputMessage> {
    loop {
        if can.rfr[0].read().fmp().bits() == 0 {
            return None;
        }

        let msg = CanRxMsg {
            stid: can.rx[0].rir.read().stid().bits(),
            exid: can.rx[0].rir.read().exid().bits(),
            ide: can.rx[0].rir.read().ide().bit(),
            dlc: can.rx[0].rdtr.read().dlc().bits(),
            data: [
                can.rx[0].rdlr.read().data0().bits(),
                can.rx[0].rdlr.read().data1().bits(),
                can.rx[0].rdlr.read().data2().bits(),
                can.rx[0].rdlr.read().data3().bits(),
                can.rx[0].rdhr.read().data4().bits(),
                can.rx[0].rdhr.read().data5().bits(),
                can.rx[0].rdhr.read().data6().bits(),
                can.rx[0].rdhr.read().data7().bits(),
            ],
        };

        // Release mailbox
        can.rfr[0].write(|w| w.rfom().set_bit());
        // Wait until the mailbox has been released
        while can.rfr[0].read().rfom().bit() {}

        // Process message
        let message = process_can_message(msg);

        if message.is_some() {
            return message;
        }
    }
}

fn process_can_message(msg: CanRxMsg) -> Option<InputMessage> {
    let msg_type = (msg.stid >> 8) as u8;
    let msg_subtype = ((msg.stid & 0xf0) >> 4) as u8;

    match msg_type {
        MSG_GENERAL => {
            // General message
            match msg_subtype {
                _ => {
                    return None;
                }
            }
        }
        MSG_LOCOMOTIVE => {
            // Locomotive message
            if !msg.ide {
                // Locomotive message used extended id
                return None;
            }

            if msg_subtype == MSG_LOCO_STATUS {
                // Status message
                if msg.dlc != 4 {
                    // Locomotive status message are 4-byte long
                    return None;
                }
                let loco_address = (msg.exid % 256) as u8;
                let loco_fwd = (msg.data[0] & (1 << 7)) != 0;
                let loco_speed = msg.data[0] & 0x7F;
                let f7_0 = msg.data[1];
                let f15_8 = msg.data[2];
                let f23_16 = msg.data[3];

                return Some(InputMessage::LocomotiveStatus {
                    address: loco_address,
                    forward: loco_fwd,
                    speed: loco_speed,
                    f7_0,
                    f15_8,
                    f23_16,
                });
            } else {
                return None;
            }

            // TODO
        }
        _ => return None,
    }
}

fn transmit_can_message(can: &Can, msg: CanTxMsg) {
    // Find an empty tx mailbox
    let mut mbox = 0;
    loop {
        if mbox == 0 {
            if can.tsr.read().tme0().bit() {
                break;
            } else {
                mbox = 1;
            }
        }
        if mbox == 1 {
            if can.tsr.read().tme1().bit() {
                break;
            } else {
                mbox = 2;
            }
        }
        if mbox == 2 {
            if can.tsr.read().tme2().bit() {
                break;
            } else {
                mbox = 0;
            }
        }
    }

    // Write message to the mailbox
    can.tx[mbox]
        .tdtr
        .write(|w| unsafe { w.dlc().bits(msg.dlc) });
    can.tx[mbox].tdlr.write(|w| unsafe {
        w.data0()
            .bits(msg.data[0])
            .data1()
            .bits(msg.data[1])
            .data2()
            .bits(msg.data[2])
            .data3()
            .bits(msg.data[3])
    });
    can.tx[mbox].tdhr.write(|w| unsafe {
        w.data4()
            .bits(msg.data[4])
            .data5()
            .bits(msg.data[5])
            .data6()
            .bits(msg.data[6])
            .data7()
            .bits(msg.data[7])
    });
    can.tx[mbox].tir.write(|w| unsafe {
        w.stid()
            .bits(msg.stid)
            .exid()
            .bits(msg.exid)
            .ide()
            .bit(msg.ide)
            .rtr()
            .clear_bit()
            .txrq()
            .set_bit()
    });
}

fn transmit_emergency_power_down(can: &Can) {
    let msg = CanTxMsg {
        stid: ((MSG_GENERAL as u16) << 8)
            | ((MSG_GENERAL_EMERGENCY as u16) << 4)
            | (MSG_SENDER_REMOTE1 as u16),
        exid: 0,
        ide: false,
        dlc: 0,
        data: [0; 8],
    };

    transmit_can_message(&can, msg);
}

fn transmit_enable_power(can: &Can) {
    let msg = CanTxMsg {
        stid: ((MSG_GENERAL as u16) << 8)
            | ((MSG_GENERAL_ENABLE_POWER as u16) << 4)
            | (MSG_SENDER_REMOTE1 as u16),
        exid: 0,
        ide: false,
        dlc: 0,
        data: [0; 8],
    };

    transmit_can_message(&can, msg);
}

fn transmit_loco_speed(can: &Can, address: u8, forward: bool, speed: u8) {
    let msg = CanTxMsg {
        stid: ((MSG_LOCOMOTIVE as u16) << 8)
            | ((MSG_LOCO_SET_SPEED as u16) << 4)
            | (MSG_SENDER_REMOTE1 as u16),
        exid: address as u32,
        ide: true,
        dlc: 1,
        data: [((forward as u8) << 7) | speed, 0, 0, 0, 0, 0, 0, 0],
    };

    transmit_can_message(&can, msg);
}

fn transmit_loco_request_status(can: &Can, address: u8) {
    let msg = CanTxMsg {
        stid: ((MSG_LOCOMOTIVE as u16) << 8)
            | ((MSG_LOCO_REQ_STATUS as u16) << 4)
            | (MSG_SENDER_REMOTE1 as u16),
        exid: address as u32,
        ide: true,
        dlc: 0,
        data: [0; 8],
    };

    transmit_can_message(&can, msg);
}

fn transmit_loco_stop(can: &Can, address: u8) {
    let msg = CanTxMsg {
        stid: ((MSG_LOCOMOTIVE as u16) << 8)
            | ((MSG_LOCO_STOP as u16) << 4)
            | (MSG_SENDER_REMOTE1 as u16),
        exid: address as u32,
        ide: true,
        dlc: 0,
        data: [0; 8],
    };

    transmit_can_message(&can, msg);
}

fn transmit_loco_toggle_fn(can: &Can, address: u8, f_num: u8) {
    let f: u32 = 1 << f_num;

    let msg = CanTxMsg {
        stid: ((MSG_LOCOMOTIVE as u16) << 8)
            | ((MSG_LOCO_UPDATE_TOGGLE_FUNCTIONS as u16) << 4)
            | (MSG_SENDER_REMOTE1 as u16),
        exid: address as u32,
        ide: true,
        dlc: 3,
        data: [
            (f & 0xff) as u8,
            ((f & 0xff00) >> 8) as u8,
            ((f & 0xff0000) >> 16) as u8,
            0,
            0,
            0,
            0,
            0,
        ],
    };

    transmit_can_message(&can, msg);
}

impl OutputMessage {
    pub fn send(&self, can: &Can) {
        match self {
            OutputMessage::EmergencyPowerDown => transmit_emergency_power_down(can),
            OutputMessage::EnablePower => transmit_enable_power(can),
            OutputMessage::LocomotiveRequestStatus { address } => {
                transmit_loco_request_status(can, *address)
            }
            OutputMessage::LocomotiveStop { address } => transmit_loco_stop(can, *address),
            OutputMessage::LocomotiveSpeed {
                address,
                forward,
                speed,
            } => transmit_loco_speed(can, *address, *forward, *speed),
            OutputMessage::LocomotiveToggleFn {
                address,
                fonction_num,
            } => transmit_loco_toggle_fn(can, *address, *fonction_num),
        }
    }
}
