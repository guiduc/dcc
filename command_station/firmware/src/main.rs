#![no_std]
#![no_main]

extern crate panic_halt;

mod board;
mod can;
mod dcc;
mod timer;
mod uart;

use core::sync::atomic::{AtomicBool, Ordering};
use stm32f4xx_hal as hal;

use crate::hal::{prelude::*, stm32};

const ZERO_HALF_PERIOD: u32 = 8400;
const ONE_HALF_PERIOD: u32 = 4872;

static NEXT_BIT: AtomicBool = AtomicBool::new(true); // Value of the next bit to send
static CURRENT_PHASE: AtomicBool = AtomicBool::new(false); // False = first part, true = second part of the bit

enum State {
    Speed,
    F0_4,
    F5_8,
    F9_12,
}

#[rtic::app(device = stm32f4xx_hal::stm32, peripherals = true)]
const APP: () = {
    struct Resources {
        dcc_timer: timer::DCCTimer,
        uart: uart::Uart,
        can: can::Can,
        status_led_pin: board::StatusLedPin,
        error_led_pin: board::ErrorLedPin,
        driver_en_pin: board::DriverEnPin,
        driver_inv_pin: board::DriverInvPin,
        locomotive_pool: dcc::LocomotivePool,
    }

    #[init(spawn = [gen_next_bit_task])]
    fn init(cx: init::Context) -> init::LateResources {
        // Data structures initialization
        let mut locomotive_pool: dcc::LocomotivePool = Default::default();
        let loco3 = dcc::Locomotive::new(3);
        locomotive_pool.locomotives[0] = Some(loco3);

        // Peripherals initialization
        let dp: stm32::Peripherals = cx.device;

        // Clocks init
        let rcc = dp.RCC.constrain();
        let clocks = board::setup_clocks(rcc);

        // GPIO init
        let gpioa = dp.GPIOA.split();
        let gpiob = dp.GPIOB.split();
        let gpioc = dp.GPIOC.split();

        let pins = board::pin_init(gpioa, gpiob, gpioc);
        let mut status_led_pin = pins.status_led_pin;
        let mut error_led_pin = pins.error_led_pin;
        let mut driver_en_pin = pins.driver_en_pin;
        let driver_inv_pin = pins.driver_inv_pin;
        let can_stby_pin = pins.can_stby_pin;

        // USART
        let uart = uart::uart_init(dp.USART1, pins.uart_tx_pin, pins.uart_rx_pin, &clocks);

        // CAN
        let can = dp.CAN1;
        can::can_init(&can, can_stby_pin);

        // Timer
        let dcc_timer = dp.TIM2;
        timer::timer_setup(&dcc_timer);
        timer::timer_set_period(&dcc_timer, ONE_HALF_PERIOD);
        timer::timer_start(&dcc_timer);

        // DCC signal is valid
        driver_en_pin.set_high().unwrap();

        status_led_pin.set_low().unwrap();
        error_led_pin.set_high().unwrap();

        init::LateResources {
            dcc_timer,
            uart,
            can,
            status_led_pin,
            error_led_pin,
            driver_en_pin,
            driver_inv_pin,
            locomotive_pool,
        }
    }

    #[task(binds = TIM2, resources = [dcc_timer, driver_inv_pin], spawn = [gen_next_bit_task], priority = 3)]
    fn dcc_timer_task(cx: dcc_timer_task::Context) {
        let dcc_timer: &mut timer::DCCTimer = cx.resources.dcc_timer;
        let driver_inv_pin: &mut board::DriverInvPin = cx.resources.driver_inv_pin;

        timer::timer_clear_int(dcc_timer);

        if CURRENT_PHASE.load(Ordering::Relaxed) {
            // No race condition (this bit is only used in the interrupt handler)
            // End of the second phase of the bit

            // Set INV bit value to high for the first part of the next bit
            driver_inv_pin.set_high().unwrap();

            // Reschedule the timer according to NEXT_BIT
            if NEXT_BIT.load(Ordering::Relaxed) {
                timer::timer_set_period(dcc_timer, ONE_HALF_PERIOD);
            } else {
                timer::timer_set_period(dcc_timer, ZERO_HALF_PERIOD);
            }

            CURRENT_PHASE.store(false, Ordering::Relaxed);

            // Launch the task to compute the next bit
            cx.spawn.gen_next_bit_task().unwrap();
        } else {
            // End of the first phase of the bit

            // Set the INV bit value to low for the second part of the current bit
            driver_inv_pin.set_low().unwrap();

            CURRENT_PHASE.store(true, Ordering::Relaxed);
        }
    }

    #[task(binds = CAN1_RX0, resources = [can, locomotive_pool, driver_en_pin, status_led_pin], priority = 1)]
    fn can_rx_task(mut cx: can_rx_task::Context) {
        let can: &mut can::Can = cx.resources.can;
        let driver_en_pin: &mut board::DriverEnPin = cx.resources.driver_en_pin;
        let status_led_pin: &mut board::StatusLedPin = cx.resources.status_led_pin;

        while let Some(message) = can::receive_can_message(can) {
            match message {
                can::CanMessage::EmergencyPowerDown => {
                    // Disable the driver
                    driver_en_pin.set_low().unwrap();
                    status_led_pin.set_high().unwrap();
                    cx.resources.locomotive_pool.lock(|locomotive_pool| {
                        locomotive_pool.reset_all_locomotives();
                    });
                }
                can::CanMessage::EnablePower => {
                    // Enable the driver
                    driver_en_pin.set_high().unwrap();
                    status_led_pin.set_low().unwrap();
                }
                can::CanMessage::LocomotiveEStop { address } => {
                    cx.resources.locomotive_pool.lock(|locomotive_pool| {
                        if let Some(locomotive_idx) = locomotive_pool.find_locomotive_idx(address) {
                            locomotive_pool.locomotives[locomotive_idx]
                                .as_mut()
                                .unwrap()
                                .stop();

                            can::transmit_loco_status(
                                can,
                                locomotive_pool.locomotives[locomotive_idx]
                                    .as_ref()
                                    .unwrap(),
                            );
                        }
                    });
                }
                can::CanMessage::LocomotiveStop { address } => {
                    cx.resources.locomotive_pool.lock(|locomotive_pool| {
                        if let Some(locomotive_idx) = locomotive_pool.find_locomotive_idx(address) {
                            locomotive_pool.locomotives[locomotive_idx]
                                .as_mut()
                                .unwrap()
                                .stop();

                            can::transmit_loco_status(
                                can,
                                locomotive_pool.locomotives[locomotive_idx]
                                    .as_ref()
                                    .unwrap(),
                            );
                        }
                    });
                }
                can::CanMessage::LocomotiveSpeed {
                    address,
                    forward,
                    speed,
                } => {
                    cx.resources.locomotive_pool.lock(|locomotive_pool| {
                        if let Some(locomotive_idx) = locomotive_pool.find_locomotive_idx(address) {
                            locomotive_pool.locomotives[locomotive_idx]
                                .as_mut()
                                .unwrap()
                                .set_speed(speed);
                            locomotive_pool.locomotives[locomotive_idx]
                                .as_mut()
                                .unwrap()
                                .set_direction(forward);

                            can::transmit_loco_status(
                                can,
                                locomotive_pool.locomotives[locomotive_idx]
                                    .as_ref()
                                    .unwrap(),
                            );
                        }
                    });
                }
                can::CanMessage::LocomotiveSetFunctions {
                    address,
                    f7_0,
                    f15_8,
                    f23_16,
                } => {
                    cx.resources.locomotive_pool.lock(|locomotive_pool| {
                        if let Some(locomotive_idx) = locomotive_pool.find_locomotive_idx(address) {
                            locomotive_pool.locomotives[locomotive_idx]
                                .as_mut()
                                .unwrap()
                                .set_functions(f7_0, f15_8, f23_16);

                            can::transmit_loco_status(
                                can,
                                locomotive_pool.locomotives[locomotive_idx]
                                    .as_ref()
                                    .unwrap(),
                            );
                        }
                    });
                }
                can::CanMessage::LocomotiveUpdateSetFunctions {
                    address,
                    f7_0,
                    f15_8,
                    f23_16,
                } => {
                    cx.resources.locomotive_pool.lock(|locomotive_pool| {
                        if let Some(locomotive_idx) = locomotive_pool.find_locomotive_idx(address) {
                            locomotive_pool.locomotives[locomotive_idx]
                                .as_mut()
                                .unwrap()
                                .update_functions(dcc::UpdateOperation::Set, f7_0, f15_8, f23_16);

                            can::transmit_loco_status(
                                can,
                                locomotive_pool.locomotives[locomotive_idx]
                                    .as_ref()
                                    .unwrap(),
                            );
                        }
                    });
                }
                can::CanMessage::LocomotiveUpdateClearFunctions {
                    address,
                    f7_0,
                    f15_8,
                    f23_16,
                } => {
                    cx.resources.locomotive_pool.lock(|locomotive_pool| {
                        if let Some(locomotive_idx) = locomotive_pool.find_locomotive_idx(address) {
                            locomotive_pool.locomotives[locomotive_idx]
                                .as_mut()
                                .unwrap()
                                .update_functions(dcc::UpdateOperation::Clear, f7_0, f15_8, f23_16);

                            can::transmit_loco_status(
                                can,
                                locomotive_pool.locomotives[locomotive_idx]
                                    .as_ref()
                                    .unwrap(),
                            );
                        }
                    });
                }
                can::CanMessage::LocomotiveUpdateToggleFunctions {
                    address,
                    f7_0,
                    f15_8,
                    f23_16,
                } => {
                    cx.resources.locomotive_pool.lock(|locomotive_pool| {
                        if let Some(locomotive_idx) = locomotive_pool.find_locomotive_idx(address) {
                            locomotive_pool.locomotives[locomotive_idx]
                                .as_mut()
                                .unwrap()
                                .update_functions(
                                    dcc::UpdateOperation::Toggle,
                                    f7_0,
                                    f15_8,
                                    f23_16,
                                );

                            can::transmit_loco_status(
                                can,
                                locomotive_pool.locomotives[locomotive_idx]
                                    .as_ref()
                                    .unwrap(),
                            );
                        }
                    });
                }
                can::CanMessage::LocomotiveRequestStatus { address } => {
                    cx.resources.locomotive_pool.lock(|locomotive_pool| {
                        if let Some(locomotive_idx) = locomotive_pool.find_locomotive_idx(address) {
                            can::transmit_loco_status(
                                can,
                                locomotive_pool.locomotives[locomotive_idx]
                                    .as_ref()
                                    .unwrap(),
                            );
                        } else {
                            can::transmit_unknown_loco_status(can, address);
                        }
                    });
                }
                _ => {}
            }
        }
    }

    #[task(binds = USART1, resources = [uart, locomotive_pool, driver_en_pin, status_led_pin], priority = 1)]
    fn uart_task(mut cx: uart_task::Context) {
        static mut LOCOMOTIVE_ADDRESS: u8 = 3; // TODO
        let uart: &mut uart::Uart = cx.resources.uart;
        let driver_en_pin: &mut board::DriverEnPin = cx.resources.driver_en_pin;
        let status_led_pin: &mut board::StatusLedPin = cx.resources.status_led_pin;

        let c = uart.read();
        if let Ok(ch) = c {
            match ch {
                b'i' => {
                    cx.resources.locomotive_pool.lock(|locomotive_pool| {
                        if let Some(locomotive_idx) =
                            locomotive_pool.find_locomotive_idx(*LOCOMOTIVE_ADDRESS)
                        {
                            locomotive_pool.locomotives[locomotive_idx]
                                .as_mut()
                                .unwrap()
                                .inc_speed();
                        }
                    });
                }
                b'd' => {
                    cx.resources.locomotive_pool.lock(|locomotive_pool| {
                        if let Some(locomotive_idx) =
                            locomotive_pool.find_locomotive_idx(*LOCOMOTIVE_ADDRESS)
                        {
                            locomotive_pool.locomotives[locomotive_idx]
                                .as_mut()
                                .unwrap()
                                .dec_speed();
                        }
                    });
                }
                b's' => {
                    cx.resources.locomotive_pool.lock(|locomotive_pool| {
                        if let Some(locomotive_idx) =
                            locomotive_pool.find_locomotive_idx(*LOCOMOTIVE_ADDRESS)
                        {
                            locomotive_pool.locomotives[locomotive_idx]
                                .as_mut()
                                .unwrap()
                                .stop();
                        }
                    });
                }
                b'r' => {
                    cx.resources.locomotive_pool.lock(|locomotive_pool| {
                        if let Some(locomotive_idx) =
                            locomotive_pool.find_locomotive_idx(*LOCOMOTIVE_ADDRESS)
                        {
                            locomotive_pool.locomotives[locomotive_idx]
                                .as_mut()
                                .unwrap()
                                .reverse_direction();
                        }
                    });
                }
                b'0'..=b'9' => {
                    cx.resources.locomotive_pool.lock(|locomotive_pool| {
                        if let Some(locomotive_idx) =
                            locomotive_pool.find_locomotive_idx(*LOCOMOTIVE_ADDRESS)
                        {
                            locomotive_pool.locomotives[locomotive_idx]
                                .as_mut()
                                .unwrap()
                                .toggle_function((ch - b'0') as usize);
                        }
                    });
                }
                b'a' => {
                    if driver_en_pin.is_high().unwrap() {
                        // Shut down the alim
                        driver_en_pin.set_low().unwrap();
                        status_led_pin.set_high().unwrap();
                        cx.resources.locomotive_pool.lock(|locomotive_pool| {
                            locomotive_pool.reset_all_locomotives();
                        });
                    } else {
                        // Enable the driver
                        driver_en_pin.set_high().unwrap();
                        status_led_pin.set_low().unwrap();
                    }
                }
                _ => {}
            }
        }
    }

    #[task(resources = [locomotive_pool], priority = 2)]
    fn gen_next_bit_task(cx: gen_next_bit_task::Context) {
        static mut STATE: State = State::Speed;
        static mut CURRENT_LOCOMOTIVE_IDX: Option<usize> = None;
        static mut CURRENT_MESSAGE: dcc::Message = dcc::Message {
            // Idle message
            address: 0xFF,
            data: 0x00,
            crc: 0xFF,
        };
        static mut CURRENT_MESSAGE_TX_STATE: dcc::MsgTxState = dcc::MsgTxState {
            status: dcc::MsgTxStatus::Preamble,
            counter: 0,
        };

        let locomotive_pool: &mut dcc::LocomotivePool = cx.resources.locomotive_pool;

        let (next_bit, finished) = CURRENT_MESSAGE.get_next_bit(CURRENT_MESSAGE_TX_STATE);
        NEXT_BIT.store(next_bit, Ordering::Relaxed);

        if finished {
            CURRENT_MESSAGE_TX_STATE.reset();

            let res = locomotive_pool.find_next_locomotive_idx(*CURRENT_LOCOMOTIVE_IDX);
            let wrapped = res.1;
            *CURRENT_LOCOMOTIVE_IDX = res.0;

            match CURRENT_LOCOMOTIVE_IDX {
                None => CURRENT_MESSAGE.build_idle(),
                Some(idx) => {
                    if wrapped {
                        match STATE {
                            State::Speed => *STATE = State::F0_4,
                            State::F0_4 => *STATE = State::F5_8,
                            State::F5_8 => *STATE = State::F9_12,
                            State::F9_12 => *STATE = State::Speed,
                        }
                    }

                    match STATE {
                        State::Speed => CURRENT_MESSAGE
                            .build_speed(locomotive_pool.locomotives[*idx].as_ref().unwrap()),
                        State::F0_4 => CURRENT_MESSAGE
                            .build_f0f4(locomotive_pool.locomotives[*idx].as_ref().unwrap()),
                        State::F5_8 => CURRENT_MESSAGE
                            .build_f5f8(locomotive_pool.locomotives[*idx].as_ref().unwrap()),
                        State::F9_12 => CURRENT_MESSAGE
                            .build_f9f12(locomotive_pool.locomotives[*idx].as_ref().unwrap()),
                    }
                }
            }
        }
    }

    // This is for software tasks
    extern "C" {
        fn UART5();
    }
};
