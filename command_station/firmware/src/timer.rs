use crate::hal::stm32;
use stm32f4xx_hal as hal;

pub type DCCTimer = stm32::TIM2;

pub fn timer_setup(tim: &DCCTimer) {
    let rcc = unsafe { &(*stm32::RCC::ptr()) };
    rcc.apb1enr.modify(|_, w| w.tim2en().set_bit());
    rcc.apb1rstr.modify(|_, w| w.tim2rst().set_bit());
    rcc.apb1rstr.modify(|_, w| w.tim2rst().clear_bit());

    // The APB1 clock frequency is 42 MHz
    // But as APB1 prescaler is > 1, the timer frequency is double
    // So base timer frequency => 84 MHz
    tim.psc.write(|w| w.psc().bits(0));
    //tim2.psc.write(|w| w.psc().bits(21000-1));
    // 84 M / 21 k => 4 kHz
}

pub fn timer_start(tim: &DCCTimer) {
    tim.cr1.modify(|_, w| w.cen().set_bit());

    // Enable Update Interrupt
    tim.dier.write(|w| w.uie().set_bit());

    stm32::NVIC::unpend(hal::stm32::Interrupt::TIM2);

    unsafe {
        stm32::NVIC::unmask(hal::stm32::Interrupt::TIM2);
    }
}

pub fn timer_set_period(tim: &DCCTimer, period: u32) {
    tim.arr.write(|w| w.arr().bits(period - 1));
}

pub fn timer_clear_int(tim: &DCCTimer) {
    tim.sr.write(|w| w.uif().clear_bit());
}
