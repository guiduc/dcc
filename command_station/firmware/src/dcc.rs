const NB_PREAMBLE_BITS: usize = 16;

pub struct Message {
    pub address: u8,
    pub data: u8,
    pub crc: u8,
}

impl Default for Message {
    fn default() -> Self {
        Message {
            address: 0xFF,
            data: 0x00,
            crc: 0xFF,
        }
    }
}

impl Message {
    fn compute_crc(&mut self) {
        self.crc = self.address ^ self.data;
    }

    pub fn build_idle(&mut self) {
        self.address = 0xFF;
        self.data = 0x00;
        self.compute_crc();
    }

    pub fn build_broadcast_stop(&mut self) {
        self.address = 0;
        self.data = 0b0100_0000;
        self.compute_crc();
    }

    pub fn build_broadcast_estop(&mut self) {
        self.address = 0;
        self.data = 0b0100_0001;
        self.compute_crc();
    }

    pub fn build_f0f4(&mut self, locomotive: &Locomotive) {
        self.address = locomotive.address;
        self.data = 0b1000_0000
            | (locomotive.functions[1] as u8)
            | ((locomotive.functions[2] as u8) << 1)
            | ((locomotive.functions[3] as u8) << 2)
            | ((locomotive.functions[4] as u8) << 3)
            | ((locomotive.functions[0] as u8) << 4);
        self.compute_crc();
    }

    pub fn build_f5f8(&mut self, locomotive: &Locomotive) {
        self.address = locomotive.address;
        self.data = 0b1011_0000
            | (locomotive.functions[5] as u8)
            | ((locomotive.functions[6] as u8) << 1)
            | ((locomotive.functions[7] as u8) << 2)
            | ((locomotive.functions[8] as u8) << 3);
        self.compute_crc();
    }

    pub fn build_f9f12(&mut self, locomotive: &Locomotive) {
        self.address = locomotive.address;
        self.data = 0b1010_0000
            | (locomotive.functions[9] as u8)
            | ((locomotive.functions[10] as u8) << 1)
            | ((locomotive.functions[11] as u8) << 2)
            | ((locomotive.functions[12] as u8) << 3);
        self.compute_crc();
    }

    pub fn build_speed(&mut self, locomotive: &Locomotive) {
        self.address = locomotive.address;

        let speed: u8 = if locomotive.speed == 0 {
            0
        } else {
            locomotive.speed.min(28) + 3
        };

        let speed_high = (speed & 0b1_1110) >> 1; // Bits 4..1
        let speed_low = speed & 1; // Bit 0

        self.data = 0b0100_0000 | ((locomotive.forward as u8) << 5) | (speed_low << 4) | speed_high;

        self.compute_crc();
    }

    pub fn get_next_bit(&self, state: &mut MsgTxState) -> (bool, bool) {
        match &state.status {
            MsgTxStatus::Preamble => {
                state.counter += 1;
                if state.counter == NB_PREAMBLE_BITS {
                    state.status = MsgTxStatus::PacketStartBit;
                    state.counter = 0;
                }
                (true, false)
            }
            MsgTxStatus::PacketStartBit => {
                state.status = MsgTxStatus::AddressByte;
                state.counter = 0;
                (false, false)
            }
            MsgTxStatus::AddressByte => {
                let bit = (self.address & (1 << (7 - state.counter))) != 0;
                if state.counter == 7 {
                    state.status = MsgTxStatus::DataStartBit;
                    state.counter = 0;
                } else {
                    state.counter += 1;
                }
                (bit, false)
            }
            MsgTxStatus::DataStartBit => {
                state.status = MsgTxStatus::DataByte;
                state.counter = 0;
                (false, false)
            }
            MsgTxStatus::DataByte => {
                let bit = (self.data & (1 << (7 - state.counter))) != 0;
                if state.counter == 7 {
                    state.status = MsgTxStatus::ErrorStartBit;
                    state.counter = 0;
                } else {
                    state.counter += 1;
                }
                (bit, false)
            }
            MsgTxStatus::ErrorStartBit => {
                state.status = MsgTxStatus::ErrorByte;
                state.counter = 0;
                (false, false)
            }
            MsgTxStatus::ErrorByte => {
                let bit = (self.crc & (1 << (7 - state.counter))) != 0;
                if state.counter == 7 {
                    state.status = MsgTxStatus::PacketEndBit;
                    state.counter = 0;
                } else {
                    state.counter += 1;
                }
                (bit, false)
            }
            MsgTxStatus::PacketEndBit => {
                state.status = MsgTxStatus::Preamble;
                state.counter = 0;
                (true, true)
            }
        }
    }
}

pub enum MsgTxStatus {
    Preamble,
    PacketStartBit,
    AddressByte,
    DataStartBit,
    DataByte,
    ErrorStartBit,
    ErrorByte,
    PacketEndBit,
}

pub struct MsgTxState {
    pub status: MsgTxStatus,
    pub counter: usize,
}

impl Default for MsgTxState {
    fn default() -> Self {
        MsgTxState {
            status: MsgTxStatus::Preamble,
            counter: 0,
        }
    }
}

impl MsgTxState {
    pub fn reset(&mut self) {
        self.status = MsgTxStatus::Preamble;
        self.counter = 0;
    }
}

pub enum UpdateOperation {
    Set,
    Clear,
    Toggle,
}

pub struct Locomotive {
    pub address: u8,
    pub speed: u8,
    pub forward: bool,
    pub functions: [bool; 24], // 0 = FL, 1..23 = F1..23
}

impl Locomotive {
    pub fn new(address: u8) -> Locomotive {
        Locomotive {
            address,
            ..Default::default()
        }
    }

    pub fn reset(&mut self) {
        self.speed = 0;
        self.functions = Default::default();
    }

    pub fn inc_speed(&mut self) -> bool {
        if self.speed < 28 {
            self.speed += 1;
            return true;
        }
        false
    }

    pub fn dec_speed(&mut self) -> bool {
        if self.speed > 0 {
            self.speed -= 1;
            return true;
        }
        false
    }

    pub fn set_speed(&mut self, speed: u8) -> bool {
        if speed <= 28 {
            self.speed = speed;
            return true;
        }
        false
    }

    pub fn stop(&mut self) {
        self.speed = 0;
    }

    pub fn reverse_direction(&mut self) -> bool {
        if self.speed != 0 {
            return false;
        }
        self.forward = !self.forward;
        true
    }

    pub fn set_direction(&mut self, forward: bool) -> bool {
        if self.speed != 0 {
            return false;
        }
        self.forward = forward;
        true
    }

    pub fn toggle_function(&mut self, f: usize) -> bool {
        if f < self.functions.len() {
            self.functions[f] = !self.functions[f];
            return true;
        }
        false
    }

    pub fn set_functions(&mut self, f0_7: u8, f8_15: u8, f16_23: u8) {
        for i in 0..8 {
            self.functions[i] = (f0_7 & (1 << i)) != 0;
        }
        for i in 8..16 {
            self.functions[i] = (f8_15 & (1 << (i - 8))) != 0;
        }
        for i in 16..24 {
            self.functions[i] = (f16_23 & (1 << (i - 16))) != 0;
        }
    }

    pub fn update_functions(&mut self, op: UpdateOperation, f0_7: u8, f8_15: u8, f16_23: u8) {
        match op {
            UpdateOperation::Set => {
                for i in 0..8 {
                    self.functions[i] |= (f0_7 & (1 << i)) != 0;
                }
                for i in 8..16 {
                    self.functions[i] |= (f8_15 & (1 << (i - 8))) != 0;
                }
                for i in 16..24 {
                    self.functions[i] |= (f16_23 & (1 << (i - 16))) != 0;
                }
            }
            UpdateOperation::Clear => {
                for i in 0..8 {
                    self.functions[i] &= (f0_7 & (1 << i)) == 0;
                }
                for i in 8..16 {
                    self.functions[i] &= (f8_15 & (1 << (i - 8))) == 0;
                }
                for i in 16..24 {
                    self.functions[i] &= (f16_23 & (1 << (i - 16))) == 0;
                }
            }
            UpdateOperation::Toggle => {
                for i in 0..8 {
                    self.functions[i] ^= (f0_7 & (1 << i)) != 0;
                }
                for i in 8..16 {
                    self.functions[i] ^= (f8_15 & (1 << (i - 8))) != 0;
                }
                for i in 16..24 {
                    self.functions[i] ^= (f16_23 & (1 << (i - 16))) != 0;
                }
            }
        }
    }
}

impl Default for Locomotive {
    fn default() -> Self {
        Locomotive {
            address: 0,
            speed: 0,
            forward: true,
            functions: [false; 24],
        }
    }
}

#[derive(Default)]
pub struct LocomotivePool {
    pub locomotives: [Option<Locomotive>; 5],
}

impl LocomotivePool {
    pub fn find_locomotive_idx(&self, address: u8) -> Option<usize> {
        for i in 0..self.locomotives.len() {
            match self.locomotives[i] {
                Some(ref loco) => {
                    if loco.address == address {
                        return Some(i);
                    }
                }
                None => continue,
            }
        }
        None
    }

    pub fn find_next_locomotive_idx(&self, current_idx: Option<usize>) -> (Option<usize>, bool) {
        if current_idx.is_some() {
            for i in (current_idx.unwrap() + 1)..self.locomotives.len() {
                if self.locomotives[i].is_some() {
                    return (Some(i), false);
                }
            }
        }

        // Wrap from the beginning
        for i in 0..self.locomotives.len() {
            if self.locomotives[i].is_some() {
                return (Some(i), true);
            }
        }

        (None, false)
    }

    pub fn reset_all_locomotives(&mut self) {
        for l in self.locomotives.iter_mut() {
            if let Some(ref mut loco) = l {
                loco.reset();
            }
        }
    }
}
