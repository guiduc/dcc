use crate::hal::{
    prelude::*,
    rcc::Clocks,
    serial::{config, Event, Serial},
    stm32,
};
use stm32f4xx_hal as hal;

pub type Uart = Serial<stm32::USART1, (super::board::UartTxPin, super::board::UartRxPin)>;

pub fn uart_init(
    uart_dev: stm32::USART1,
    tx_pin: super::board::UartTxPin,
    rx_pin: super::board::UartRxPin,
    clocks: &Clocks,
) -> Uart {
    let usart1_config = config::Config {
        baudrate: 115_200.bps(),
        wordlength: config::WordLength::DataBits8,
        parity: config::Parity::ParityNone,
        stopbits: config::StopBits::STOP1,
    };

    let mut uart = Serial::usart1(uart_dev, (tx_pin, rx_pin), usart1_config, *clocks).unwrap();
    uart.listen(Event::Rxne);

    stm32::NVIC::unpend(hal::stm32::Interrupt::USART1);
    unsafe {
        stm32::NVIC::unmask(hal::stm32::Interrupt::USART1);
    }

    uart
}
