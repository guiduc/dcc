// Pins
// PA1 : DRIVER_EN_SENSE (Input Floating)
// PA2 : ERROR_LED (Output Open-Drain)
// PA3 : DRIVER_EN (Output PP)
// PA5 : SPI1_SCK (AF5)
// PA6 : SPI1_MISO (AF5)
// PA7 : SPI1_MOSI (AF5)
// PA9 : USART1_TX (AF7)
// PA10 : USART1_RX (AF7)

// PB1 : EXT3
// PB8 : CAN1_RX (AF9)
// PB9 : CAN1_TX (AF9)
// PB10 : DRIVER_SF (Input Floating)
// PB11 : CAN_STBY (Output PP)

// PC0 : EXT2
// PC1 : EXT1
// PC2 : DRIVER_INV (Output PP)
// PC3 : CURRENT_SENSE (Analog)
// PC5 : STATUS_LED (Output Open-Drain)

use crate::hal::{
    gpio::{gpioa, gpiob, gpioc, Alternate, OpenDrain, Output, PushPull, Speed, AF7, AF9},
    prelude::*,
    rcc::{Clocks, Rcc},
};

pub type StatusLedPin = gpioc::PC5<Output<OpenDrain>>;
pub type ErrorLedPin = gpioa::PA2<Output<OpenDrain>>;
pub type DriverEnPin = gpioa::PA3<Output<PushPull>>;
pub type DriverInvPin = gpioc::PC2<Output<PushPull>>;
pub type UartTxPin = gpioa::PA9<Alternate<AF7>>;
pub type UartRxPin = gpioa::PA10<Alternate<AF7>>;
pub type CanStbyPin = gpiob::PB11<Output<PushPull>>;
pub type CanRxPin = gpiob::PB8<Alternate<AF9>>;
pub type CanTxPin = gpiob::PB9<Alternate<AF9>>;

pub struct Pins {
    pub status_led_pin: StatusLedPin,
    pub error_led_pin: ErrorLedPin,
    pub driver_en_pin: DriverEnPin,
    pub driver_inv_pin: DriverInvPin,
    pub uart_tx_pin: UartTxPin,
    pub uart_rx_pin: UartRxPin,
    pub can_stby_pin: CanStbyPin,
    pub can_rx_pin: CanRxPin,
    pub can_tx_pin: CanTxPin,
}

pub fn setup_clocks(rcc: Rcc) -> Clocks {
    rcc.cfgr
        .use_hse(8.mhz())
        .sysclk(168.mhz())
        .pclk1(42.mhz())
        .pclk2(84.mhz())
        .freeze()
}

pub fn pin_init(gpioa: gpioa::Parts, gpiob: gpiob::Parts, gpioc: gpioc::Parts) -> Pins {
    let mut status_led_pin = gpioc.pc5.into_open_drain_output();
    status_led_pin.set_high().unwrap();
    let mut error_led_pin = gpioa.pa2.into_open_drain_output();
    error_led_pin.set_high().unwrap();

    let mut driver_en_pin = gpioa.pa3.into_push_pull_output();
    driver_en_pin.set_low().unwrap();
    let mut driver_inv_pin = gpioc.pc2.into_push_pull_output().set_speed(Speed::VeryHigh);
    driver_inv_pin.set_low().unwrap();

    let uart_tx_pin = gpioa.pa9.into_alternate_af7();
    let uart_rx_pin = gpioa.pa10.into_alternate_af7();

    let mut can_stby_pin = gpiob.pb11.into_push_pull_output();
    can_stby_pin.set_high().unwrap();

    let can_rx_pin = gpiob.pb8.into_alternate_af9();
    let can_tx_pin = gpiob.pb9.into_alternate_af9();

    Pins {
        status_led_pin,
        error_led_pin,
        driver_en_pin,
        driver_inv_pin,
        uart_tx_pin,
        uart_rx_pin,
        can_stby_pin,
        can_rx_pin,
        can_tx_pin,
    }
}
