# Command station firmware

This directory contains the embedded software that runs on the microcontroler of the [DCC command station](https://www.guiduc.org/projects/dcc/command_station/).

This firmware is written in Rust.

## Roadmap

### 0.3

- Add the possibility to dynamically manage locomotives (add, remove...)

## License

Copyright 2020 Guillaume Duc <guillaume@guiduc.org>

Licensed under either of

 * Apache License, Version 2.0, ([LICENSE-APACHE](../../LICENSE-APACHE) or http://www.apache.org/licenses/LICENSE-2.0)
 * MIT license ([LICENSE-MIT](../../LICENSE-MIT) or http://opensource.org/licenses/MIT)

at your option.
