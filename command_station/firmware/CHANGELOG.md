# Change Log

## v0.2.0 - 2020-08-30

### Changed

- Use of [RTIC](https://docs.rs/cortex-m-rtic) framework

## v0.1.0 - 2020-08-23

### Added

- Support for a single locomotive (direction, speed, functions) at a fixed address
- Support for a remote control avec CAN bus
- Support for receiving commands from a PC via the UART
