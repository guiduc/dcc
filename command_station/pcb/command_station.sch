EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title "DCC Command Station"
Date "2020-03-08"
Rev "1.0"
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L Connector:Screw_Terminal_01x02 J1
U 1 1 5E3E9FD8
P 950 1000
F 0 "J1" H 868 767 50  0000 C CNN
F 1 "Power" H 1150 950 50  0000 C CNN
F 2 "custom:bornier_2P" H 950 1000 50  0001 C CNN
F 3 "~" H 950 1000 50  0001 C CNN
	1    950  1000
	-1   0    0    1   
$EndComp
$Comp
L power:GND #PWR0101
U 1 1 5E3EB69E
P 2150 1250
F 0 "#PWR0101" H 2150 1000 50  0001 C CNN
F 1 "GND" H 2155 1077 50  0000 C CNN
F 2 "" H 2150 1250 50  0001 C CNN
F 3 "" H 2150 1250 50  0001 C CNN
	1    2150 1250
	1    0    0    -1  
$EndComp
$Comp
L power:+15V #PWR0102
U 1 1 5E3EC365
P 2150 850
F 0 "#PWR0102" H 2150 700 50  0001 C CNN
F 1 "+15V" H 2165 1023 50  0000 C CNN
F 2 "" H 2150 850 50  0001 C CNN
F 3 "" H 2150 850 50  0001 C CNN
	1    2150 850 
	1    0    0    -1  
$EndComp
$Comp
L Device:Fuse F1
U 1 1 5E3ECE24
P 1900 900
F 0 "F1" V 1703 900 50  0000 C CNN
F 1 "Fuse" V 1794 900 50  0000 C CNN
F 2 "custom:fuse_holder" V 1830 900 50  0001 C CNN
F 3 "~" H 1900 900 50  0001 C CNN
	1    1900 900 
	0    1    1    0   
$EndComp
$Comp
L Diode:1N47xxA D1
U 1 1 5E3EFA70
P 2150 1050
F 0 "D1" V 2104 1129 50  0000 L CNN
F 1 "1N4747A" V 2195 1129 50  0000 L CNN
F 2 "Diode_THT:D_DO-41_SOD81_P10.16mm_Horizontal" H 2150 875 50  0001 C CNN
F 3 "https://www.vishay.com/docs/85816/1n4728a.pdf" H 2150 1050 50  0001 C CNN
	1    2150 1050
	0    1    1    0   
$EndComp
Wire Wire Line
	2050 900  2150 900 
Wire Wire Line
	2150 850  2150 900 
Connection ~ 2150 900 
Wire Wire Line
	1800 1200 2150 1200
Wire Wire Line
	1800 1000 1800 1200
Wire Wire Line
	2150 1200 2150 1250
Connection ~ 2150 1200
$Comp
L custom:Pololu_2858 U2
U 1 1 5E3F80CB
P 3150 900
F 0 "U2" H 3175 1165 50  0000 C CNN
F 1 "Pololu_2858" H 3175 1074 50  0000 C CNN
F 2 "custom:Pololu_2858" H 3600 450 50  0001 C CNN
F 3 "" H 3600 450 50  0001 C CNN
	1    3150 900 
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR0103
U 1 1 5E3F9542
P 3150 1350
F 0 "#PWR0103" H 3150 1100 50  0001 C CNN
F 1 "GND" H 3155 1177 50  0000 C CNN
F 2 "" H 3150 1350 50  0001 C CNN
F 3 "" H 3150 1350 50  0001 C CNN
	1    3150 1350
	1    0    0    -1  
$EndComp
Wire Wire Line
	2150 900  2800 900 
NoConn ~ 2800 1000
$Comp
L power:+5V #PWR0104
U 1 1 5E3F9EEB
P 3700 850
F 0 "#PWR0104" H 3700 700 50  0001 C CNN
F 1 "+5V" H 3715 1023 50  0000 C CNN
F 2 "" H 3700 850 50  0001 C CNN
F 3 "" H 3700 850 50  0001 C CNN
	1    3700 850 
	1    0    0    -1  
$EndComp
$Comp
L Device:R R1
U 1 1 5E3FA60D
P 4250 1050
F 0 "R1" H 4320 1096 50  0000 L CNN
F 1 "1.5k" H 4320 1005 50  0000 L CNN
F 2 "Resistor_THT:R_Axial_DIN0207_L6.3mm_D2.5mm_P10.16mm_Horizontal" V 4180 1050 50  0001 C CNN
F 3 "~" H 4250 1050 50  0001 C CNN
	1    4250 1050
	1    0    0    -1  
$EndComp
Wire Wire Line
	4250 1200 4100 1200
$Comp
L Device:LED D2
U 1 1 5E3FB12C
P 4250 1350
F 0 "D2" V 4289 1233 50  0000 R CNN
F 1 "G 2.2V 2mA" V 4198 1233 50  0000 R CNN
F 2 "LED_THT:LED_D5.0mm" H 4250 1350 50  0001 C CNN
F 3 "~" H 4250 1350 50  0001 C CNN
	1    4250 1350
	0    -1   -1   0   
$EndComp
Connection ~ 4250 1200
$Comp
L power:GND #PWR0105
U 1 1 5E3FB924
P 4250 1500
F 0 "#PWR0105" H 4250 1250 50  0001 C CNN
F 1 "GND" H 4255 1327 50  0000 C CNN
F 2 "" H 4250 1500 50  0001 C CNN
F 3 "" H 4250 1500 50  0001 C CNN
	1    4250 1500
	1    0    0    -1  
$EndComp
$Comp
L custom:STM32-H405 U1
U 1 1 5E3FEB43
P 2300 3700
F 0 "U1" H 2300 5175 50  0000 C CNN
F 1 "STM32-H405" H 2300 5084 50  0000 C CNN
F 2 "custom:STM32_H405" H 2400 2350 50  0001 C CNN
F 3 "" H 2400 2350 50  0001 C CNN
	1    2300 3700
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR0106
U 1 1 5E401F9E
P 1550 3150
F 0 "#PWR0106" H 1550 2900 50  0001 C CNN
F 1 "GND" H 1555 2977 50  0000 C CNN
F 2 "" H 1550 3150 50  0001 C CNN
F 3 "" H 1550 3150 50  0001 C CNN
	1    1550 3150
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR0107
U 1 1 5E4031CA
P 4000 3000
F 0 "#PWR0107" H 4000 2750 50  0001 C CNN
F 1 "GND" H 4005 2827 50  0000 C CNN
F 2 "" H 4000 3000 50  0001 C CNN
F 3 "" H 4000 3000 50  0001 C CNN
	1    4000 3000
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR0108
U 1 1 5E403F82
P 2950 5000
F 0 "#PWR0108" H 2950 4750 50  0001 C CNN
F 1 "GND" H 2955 4827 50  0000 C CNN
F 2 "" H 2950 5000 50  0001 C CNN
F 3 "" H 2950 5000 50  0001 C CNN
	1    2950 5000
	1    0    0    -1  
$EndComp
Wire Wire Line
	2750 4850 2950 4850
Wire Wire Line
	2950 4850 2950 5000
$Comp
L power:+5V #PWR0109
U 1 1 5E407B60
P 3050 4800
F 0 "#PWR0109" H 3050 4650 50  0001 C CNN
F 1 "+5V" H 3065 4973 50  0000 C CNN
F 2 "" H 3050 4800 50  0001 C CNN
F 3 "" H 3050 4800 50  0001 C CNN
	1    3050 4800
	1    0    0    -1  
$EndComp
Wire Wire Line
	2750 4950 3050 4950
Wire Wire Line
	3050 4950 3050 4800
$Comp
L custom:Pololu_1212 U3
U 1 1 5E41AB04
P 6550 3500
F 0 "U3" H 6550 4275 50  0000 C CNN
F 1 "Pololu_1212" H 6550 4184 50  0000 C CNN
F 2 "custom:Pololu_1212" H 6550 3500 50  0001 C CNN
F 3 "" H 6550 3500 50  0001 C CNN
	1    6550 3500
	1    0    0    -1  
$EndComp
$Comp
L power:+5V #PWR0112
U 1 1 5E41EE0B
P 5850 2900
F 0 "#PWR0112" H 5850 2750 50  0001 C CNN
F 1 "+5V" H 5865 3073 50  0000 C CNN
F 2 "" H 5850 2900 50  0001 C CNN
F 3 "" H 5850 2900 50  0001 C CNN
	1    5850 2900
	1    0    0    -1  
$EndComp
Wire Wire Line
	6100 3150 5850 3150
Wire Wire Line
	5850 3150 5850 2900
$Comp
L power:GND #PWR0113
U 1 1 5E41FA8A
P 7050 3750
F 0 "#PWR0113" H 7050 3500 50  0001 C CNN
F 1 "GND" H 7055 3577 50  0000 C CNN
F 2 "" H 7050 3750 50  0001 C CNN
F 3 "" H 7050 3750 50  0001 C CNN
	1    7050 3750
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR0114
U 1 1 5E4204E0
P 5700 4150
F 0 "#PWR0114" H 5700 3900 50  0001 C CNN
F 1 "GND" H 5705 3977 50  0000 C CNN
F 2 "" H 5700 4150 50  0001 C CNN
F 3 "" H 5700 4150 50  0001 C CNN
	1    5700 4150
	1    0    0    -1  
$EndComp
Wire Wire Line
	5700 3050 6100 3050
Wire Wire Line
	6100 3950 5850 3950
Wire Wire Line
	5850 3950 5850 3450
Connection ~ 5850 3150
Wire Wire Line
	6100 3450 5850 3450
Connection ~ 5850 3450
Wire Wire Line
	5850 3450 5850 3250
Wire Wire Line
	5700 3050 5700 3350
Wire Wire Line
	6100 3550 5700 3550
Connection ~ 5700 3550
Wire Wire Line
	5700 3550 5700 4150
Wire Wire Line
	6100 3250 5850 3250
Connection ~ 5850 3250
Wire Wire Line
	5850 3250 5850 3150
Wire Wire Line
	6100 3350 5700 3350
Connection ~ 5700 3350
Wire Wire Line
	5700 3350 5700 3550
Wire Wire Line
	7000 3450 7050 3450
Wire Wire Line
	7050 3450 7050 3750
$Comp
L Connector:Screw_Terminal_01x02 J2
U 1 1 5E42D91A
P 7500 3550
F 0 "J2" H 7500 3700 50  0000 L CNN
F 1 "To rails" H 7750 3500 50  0000 C CNN
F 2 "custom:bornier_2P" H 7500 3550 50  0001 C CNN
F 3 "~" H 7500 3550 50  0001 C CNN
	1    7500 3550
	1    0    0    -1  
$EndComp
Wire Wire Line
	7000 3550 7300 3550
Wire Wire Line
	7000 3650 7300 3650
NoConn ~ 6100 3750
Text GLabel 4700 3850 0    50   Input ~ 0
DRIVER_EN
Text GLabel 5650 3650 0    50   Input ~ 0
DRIVER_SF
$Comp
L Interface_CAN_LIN:MCP2562-E-P U4
U 1 1 5E4464F6
P 7450 5050
F 0 "U4" H 7800 4700 50  0000 C CNN
F 1 "MCP2562-E-P" H 7450 5050 50  0000 C CNN
F 2 "Package_DIP:DIP-8_W7.62mm" H 7450 4550 50  0001 C CIN
F 3 "http://ww1.microchip.com/downloads/en/DeviceDoc/25167A.pdf" H 7450 5050 50  0001 C CNN
	1    7450 5050
	1    0    0    -1  
$EndComp
$Comp
L power:+3.3V #PWR0115
U 1 1 5E44751C
P 2950 2850
F 0 "#PWR0115" H 2950 2700 50  0001 C CNN
F 1 "+3.3V" H 2965 3023 50  0000 C CNN
F 2 "" H 2950 2850 50  0001 C CNN
F 3 "" H 2950 2850 50  0001 C CNN
	1    2950 2850
	1    0    0    -1  
$EndComp
Wire Wire Line
	2750 2850 2950 2850
$Comp
L power:+3.3V #PWR0116
U 1 1 5E4499CA
P 1650 2300
F 0 "#PWR0116" H 1650 2150 50  0001 C CNN
F 1 "+3.3V" H 1665 2473 50  0000 C CNN
F 2 "" H 1650 2300 50  0001 C CNN
F 3 "" H 1650 2300 50  0001 C CNN
	1    1650 2300
	1    0    0    -1  
$EndComp
Wire Wire Line
	1650 2850 1850 2850
$Comp
L custom:Pololu_1308 U5
U 1 1 5E44E60A
P 8200 1300
F 0 "U5" H 8378 1351 50  0000 L CNN
F 1 "Pololu_1308" H 8378 1260 50  0000 L CNN
F 2 "custom:Pololu_1308" H 8200 1300 50  0001 C CNN
F 3 "" H 8200 1300 50  0001 C CNN
	1    8200 1300
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR0117
U 1 1 5E44F3A0
P 7850 1600
F 0 "#PWR0117" H 7850 1350 50  0001 C CNN
F 1 "GND" H 7855 1427 50  0000 C CNN
F 2 "" H 7850 1600 50  0001 C CNN
F 3 "" H 7850 1600 50  0001 C CNN
	1    7850 1600
	1    0    0    -1  
$EndComp
Wire Wire Line
	7900 1550 7850 1550
Wire Wire Line
	7850 1550 7850 1600
NoConn ~ 7900 1050
NoConn ~ 7900 1450
NoConn ~ 7900 1350
Text GLabel 7250 1150 0    50   Input ~ 0
UART_TX
Text GLabel 7250 1250 0    50   Input ~ 0
UART_RX
Text GLabel 1450 2750 0    50   Input ~ 0
UART_TX
Wire Wire Line
	1450 2750 1850 2750
Text GLabel 1450 3050 0    50   Input ~ 0
UART_RX
$Comp
L Device:R R5
U 1 1 5E466D86
P 7650 950
F 0 "R5" H 7720 996 50  0000 L CNN
F 1 "100k" H 7720 905 50  0000 L CNN
F 2 "Resistor_THT:R_Axial_DIN0207_L6.3mm_D2.5mm_P10.16mm_Horizontal" V 7580 950 50  0001 C CNN
F 3 "~" H 7650 950 50  0001 C CNN
	1    7650 950 
	1    0    0    -1  
$EndComp
$Comp
L Device:R R4
U 1 1 5E4683BB
P 7350 950
F 0 "R4" H 7420 996 50  0000 L CNN
F 1 "100k" H 7420 905 50  0000 L CNN
F 2 "Resistor_THT:R_Axial_DIN0207_L6.3mm_D2.5mm_P10.16mm_Horizontal" V 7280 950 50  0001 C CNN
F 3 "~" H 7350 950 50  0001 C CNN
	1    7350 950 
	1    0    0    -1  
$EndComp
Wire Wire Line
	7250 1250 7350 1250
Wire Wire Line
	7350 1100 7350 1250
Connection ~ 7350 1250
Wire Wire Line
	7350 1250 7900 1250
Wire Wire Line
	7650 1100 7650 1150
Wire Wire Line
	7250 1150 7650 1150
Connection ~ 7650 1150
Wire Wire Line
	7650 1150 7900 1150
Wire Wire Line
	7350 800  7650 800 
$Comp
L power:+3.3V #PWR0118
U 1 1 5E476830
P 7350 800
F 0 "#PWR0118" H 7350 650 50  0001 C CNN
F 1 "+3.3V" H 7365 973 50  0000 C CNN
F 2 "" H 7350 800 50  0001 C CNN
F 3 "" H 7350 800 50  0001 C CNN
	1    7350 800 
	1    0    0    -1  
$EndComp
Connection ~ 7350 800 
$Comp
L Switch:SW_SPST SW1
U 1 1 5E48D976
P 1400 900
F 0 "SW1" H 1400 1050 50  0000 C CNN
F 1 "SW_SPST" H 1400 1044 50  0001 C CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x02_P2.54mm_Vertical" H 1400 900 50  0001 C CNN
F 3 "~" H 1400 900 50  0001 C CNN
	1    1400 900 
	1    0    0    -1  
$EndComp
Wire Wire Line
	1150 900  1200 900 
Wire Wire Line
	1600 900  1750 900 
Wire Wire Line
	1150 1000 1800 1000
Text GLabel 5650 4050 0    50   Input ~ 0
DRIVER_INV
Wire Wire Line
	5650 3650 6100 3650
Wire Wire Line
	5650 4050 6100 4050
$Comp
L Switch:SW_SPST SW2
U 1 1 5E4B2383
P 4950 3850
F 0 "SW2" H 4950 3993 50  0000 C CNN
F 1 "SW_SPST" H 4950 3994 50  0001 C CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x02_P2.54mm_Vertical" H 4950 3850 50  0001 C CNN
F 3 "~" H 4950 3850 50  0001 C CNN
	1    4950 3850
	1    0    0    -1  
$EndComp
Wire Wire Line
	4700 3850 4750 3850
Wire Wire Line
	5150 3850 5250 3850
Text GLabel 4700 3950 0    50   Input ~ 0
DRIVER_EN_SENSE
Wire Wire Line
	5250 3850 5250 3950
Wire Wire Line
	5250 3950 4700 3950
Connection ~ 5250 3850
Wire Wire Line
	5250 3850 6100 3850
$Comp
L Connector:8P8C J3
U 1 1 5E4D04D0
P 9850 3950
F 0 "J3" H 9520 3954 50  0000 R CNN
F 1 "8P8C" H 9520 4045 50  0000 R CNN
F 2 "custom:Connector_8P8C" V 9850 3975 50  0001 C CNN
F 3 "~" V 9850 3975 50  0001 C CNN
	1    9850 3950
	-1   0    0    1   
$EndComp
$Comp
L Connector:8P8C J4
U 1 1 5E4D1227
P 9850 5100
F 0 "J4" H 9520 5104 50  0000 R CNN
F 1 "8P8C" H 9520 5195 50  0000 R CNN
F 2 "custom:Connector_8P8C" V 9850 5125 50  0001 C CNN
F 3 "~" V 9850 5125 50  0001 C CNN
	1    9850 5100
	-1   0    0    1   
$EndComp
Text GLabel 5250 1600 0    50   Input ~ 0
STATUS_LED
$Comp
L power:+5V #PWR0119
U 1 1 5E4E05B7
P 5300 850
F 0 "#PWR0119" H 5300 700 50  0001 C CNN
F 1 "+5V" H 5315 1023 50  0000 C CNN
F 2 "" H 5300 850 50  0001 C CNN
F 3 "" H 5300 850 50  0001 C CNN
	1    5300 850 
	1    0    0    -1  
$EndComp
$Comp
L Device:R R2
U 1 1 5E4E0F9E
P 5300 1050
F 0 "R2" H 5370 1096 50  0000 L CNN
F 1 "1.5k" H 5370 1005 50  0000 L CNN
F 2 "Resistor_THT:R_Axial_DIN0207_L6.3mm_D2.5mm_P10.16mm_Horizontal" V 5230 1050 50  0001 C CNN
F 3 "~" H 5300 1050 50  0001 C CNN
	1    5300 1050
	1    0    0    -1  
$EndComp
$Comp
L Device:LED D3
U 1 1 5E4E187B
P 5300 1400
F 0 "D3" V 5339 1282 50  0000 R CNN
F 1 "G 2.2V 2mA" V 5248 1282 50  0000 R CNN
F 2 "LED_THT:LED_D5.0mm" H 5300 1400 50  0001 C CNN
F 3 "~" H 5300 1400 50  0001 C CNN
	1    5300 1400
	0    -1   -1   0   
$EndComp
Wire Wire Line
	5300 850  5300 900 
Wire Wire Line
	5300 1200 5300 1250
Wire Wire Line
	5300 1550 5300 1600
Wire Wire Line
	5300 1600 5250 1600
$Comp
L power:+5V #PWR0120
U 1 1 5E4F5AAE
P 6200 850
F 0 "#PWR0120" H 6200 700 50  0001 C CNN
F 1 "+5V" H 6215 1023 50  0000 C CNN
F 2 "" H 6200 850 50  0001 C CNN
F 3 "" H 6200 850 50  0001 C CNN
	1    6200 850 
	1    0    0    -1  
$EndComp
$Comp
L Device:R R3
U 1 1 5E4F5C97
P 6200 1050
F 0 "R3" H 6270 1096 50  0000 L CNN
F 1 "1.5k" H 6270 1005 50  0000 L CNN
F 2 "Resistor_THT:R_Axial_DIN0207_L6.3mm_D2.5mm_P10.16mm_Horizontal" V 6130 1050 50  0001 C CNN
F 3 "~" H 6200 1050 50  0001 C CNN
	1    6200 1050
	1    0    0    -1  
$EndComp
$Comp
L Device:LED D4
U 1 1 5E4F5FCB
P 6200 1400
F 0 "D4" V 6239 1282 50  0000 R CNN
F 1 "R 2V 2mA" V 6148 1282 50  0000 R CNN
F 2 "LED_THT:LED_D5.0mm" H 6200 1400 50  0001 C CNN
F 3 "~" H 6200 1400 50  0001 C CNN
	1    6200 1400
	0    -1   -1   0   
$EndComp
Text GLabel 6150 1600 0    50   Input ~ 0
ERROR_LED
Wire Wire Line
	6200 850  6200 900 
Wire Wire Line
	6200 1200 6200 1250
Wire Wire Line
	6200 1550 6200 1600
Wire Wire Line
	6200 1600 6150 1600
$Comp
L power:+5V #PWR0121
U 1 1 5E53ACD6
P 7450 4550
F 0 "#PWR0121" H 7450 4400 50  0001 C CNN
F 1 "+5V" H 7465 4723 50  0000 C CNN
F 2 "" H 7450 4550 50  0001 C CNN
F 3 "" H 7450 4550 50  0001 C CNN
	1    7450 4550
	1    0    0    -1  
$EndComp
$Comp
L Device:C C2
U 1 1 5E53B382
P 7650 4600
F 0 "C2" V 7398 4600 50  0000 C CNN
F 1 "0.1 uF" V 7489 4600 50  0000 C CNN
F 2 "Capacitor_THT:C_Rect_L7.0mm_W2.5mm_P5.00mm" H 7688 4450 50  0001 C CNN
F 3 "~" H 7650 4600 50  0001 C CNN
	1    7650 4600
	0    1    1    0   
$EndComp
Wire Wire Line
	7450 4550 7450 4600
Wire Wire Line
	7450 4600 7500 4600
Connection ~ 7450 4600
Wire Wire Line
	7450 4600 7450 4650
$Comp
L power:GND #PWR0122
U 1 1 5E53EC6B
P 7950 4600
F 0 "#PWR0122" H 7950 4350 50  0001 C CNN
F 1 "GND" H 7955 4427 50  0000 C CNN
F 2 "" H 7950 4600 50  0001 C CNN
F 3 "" H 7950 4600 50  0001 C CNN
	1    7950 4600
	1    0    0    -1  
$EndComp
$Comp
L power:+3.3V #PWR0123
U 1 1 5E540C4C
P 6100 5100
F 0 "#PWR0123" H 6100 4950 50  0001 C CNN
F 1 "+3.3V" H 6115 5273 50  0000 C CNN
F 2 "" H 6100 5100 50  0001 C CNN
F 3 "" H 6100 5100 50  0001 C CNN
	1    6100 5100
	1    0    0    -1  
$EndComp
$Comp
L Device:C C1
U 1 1 5E5431E5
P 6100 5350
F 0 "C1" H 6215 5396 50  0000 L CNN
F 1 "0.1 uF" H 6215 5305 50  0000 L CNN
F 2 "Capacitor_THT:C_Rect_L7.0mm_W2.5mm_P5.00mm" H 6138 5200 50  0001 C CNN
F 3 "~" H 6100 5350 50  0001 C CNN
	1    6100 5350
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR0124
U 1 1 5E54384D
P 6100 5550
F 0 "#PWR0124" H 6100 5300 50  0001 C CNN
F 1 "GND" H 6105 5377 50  0000 C CNN
F 2 "" H 6100 5550 50  0001 C CNN
F 3 "" H 6100 5550 50  0001 C CNN
	1    6100 5550
	1    0    0    -1  
$EndComp
Wire Wire Line
	7800 4600 7950 4600
Wire Wire Line
	6950 5150 6100 5150
Wire Wire Line
	6100 5150 6100 5100
Wire Wire Line
	6100 5150 6100 5200
Connection ~ 6100 5150
Wire Wire Line
	6100 5500 6100 5550
Text GLabel 6900 4850 0    50   Input ~ 0
CAN_TXD
Text GLabel 6900 4950 0    50   Input ~ 0
CAN_RXD
Wire Wire Line
	6900 4850 6950 4850
Wire Wire Line
	6900 4950 6950 4950
Text GLabel 6900 5250 0    50   Input ~ 0
CAN_STBY
Wire Wire Line
	6900 5250 6950 5250
$Comp
L power:GND #PWR0125
U 1 1 5E55D487
P 7450 5500
F 0 "#PWR0125" H 7450 5250 50  0001 C CNN
F 1 "GND" H 7455 5327 50  0000 C CNN
F 2 "" H 7450 5500 50  0001 C CNN
F 3 "" H 7450 5500 50  0001 C CNN
	1    7450 5500
	1    0    0    -1  
$EndComp
Wire Wire Line
	7450 5450 7450 5500
$Comp
L Device:R R6
U 1 1 5E57CA3A
P 8450 5000
F 0 "R6" H 8520 5046 50  0000 L CNN
F 1 "120" H 8520 4955 50  0000 L CNN
F 2 "Resistor_THT:R_Axial_DIN0309_L9.0mm_D3.2mm_P12.70mm_Horizontal" V 8380 5000 50  0001 C CNN
F 3 "~" H 8450 5000 50  0001 C CNN
	1    8450 5000
	1    0    0    -1  
$EndComp
$Comp
L Device:Jumper_NC_Small JP1
U 1 1 5E57E23A
P 8450 4700
F 0 "JP1" V 8450 4775 50  0000 L CNN
F 1 "Jumper_NC_Small" V 8495 4774 50  0001 L CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x02_P2.54mm_Vertical" H 8450 4700 50  0001 C CNN
F 3 "~" H 8450 4700 50  0001 C CNN
	1    8450 4700
	0    1    1    0   
$EndComp
Wire Wire Line
	7950 5150 8450 5150
Wire Wire Line
	8450 4800 8450 4850
Wire Wire Line
	7950 4950 8300 4950
Wire Wire Line
	8300 4950 8300 4550
Wire Wire Line
	8300 4550 8450 4550
Wire Wire Line
	8450 4550 8450 4600
Wire Wire Line
	8450 5150 9050 5150
Wire Wire Line
	9050 5150 9050 4900
Wire Wire Line
	9050 4900 9250 4900
Connection ~ 8450 5150
Wire Wire Line
	9450 5000 9400 5000
Wire Wire Line
	9400 5000 9400 5400
Wire Wire Line
	9400 5400 9450 5400
Wire Wire Line
	9400 5400 9400 5650
Connection ~ 9400 5400
$Comp
L power:GND #PWR0126
U 1 1 5E5927A5
P 9400 5650
F 0 "#PWR0126" H 9400 5400 50  0001 C CNN
F 1 "GND" H 9405 5477 50  0000 C CNN
F 2 "" H 9400 5650 50  0001 C CNN
F 3 "" H 9400 5650 50  0001 C CNN
	1    9400 5650
	1    0    0    -1  
$EndComp
Wire Wire Line
	8450 4550 9200 4550
Wire Wire Line
	9200 4550 9200 4800
Wire Wire Line
	9200 4800 9450 4800
Connection ~ 8450 4550
Wire Wire Line
	9200 4550 9200 3650
Wire Wire Line
	9200 3650 9450 3650
Connection ~ 9200 4550
Wire Wire Line
	9250 4900 9250 3750
Wire Wire Line
	9250 3750 9450 3750
Connection ~ 9250 4900
Wire Wire Line
	9250 4900 9450 4900
Wire Wire Line
	9400 5000 9400 4250
Wire Wire Line
	9400 3850 9450 3850
Connection ~ 9400 5000
Wire Wire Line
	9450 4250 9400 4250
Connection ~ 9400 4250
Wire Wire Line
	9400 4250 9400 3850
NoConn ~ 9450 3950
NoConn ~ 9450 4050
NoConn ~ 9450 5100
NoConn ~ 9450 5200
NoConn ~ 9450 4150
NoConn ~ 9450 5300
$Comp
L Device:Jumper_NC_Small JP3
U 1 1 5E5B8AE5
P 9200 5400
F 0 "JP3" V 9200 5200 50  0000 L CNN
F 1 "Jumper_NC_Small" V 9245 5474 50  0001 L CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x02_P2.54mm_Vertical" H 9200 5400 50  0001 C CNN
F 3 "~" H 9200 5400 50  0001 C CNN
	1    9200 5400
	0    1    1    0   
$EndComp
$Comp
L power:+5V #PWR0127
U 1 1 5E5B929A
P 9200 5250
F 0 "#PWR0127" H 9200 5100 50  0001 C CNN
F 1 "+5V" H 9215 5423 50  0000 C CNN
F 2 "" H 9200 5250 50  0001 C CNN
F 3 "" H 9200 5250 50  0001 C CNN
	1    9200 5250
	1    0    0    -1  
$EndComp
$Comp
L Device:Jumper_NC_Small JP2
U 1 1 5E5C3293
P 9050 4250
F 0 "JP2" V 9050 4050 50  0000 L CNN
F 1 "Jumper_NC_Small" V 9095 4324 50  0001 L CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x02_P2.54mm_Vertical" H 9050 4250 50  0001 C CNN
F 3 "~" H 9050 4250 50  0001 C CNN
	1    9050 4250
	0    1    1    0   
$EndComp
Wire Wire Line
	9450 4350 9050 4350
$Comp
L power:+5V #PWR0128
U 1 1 5E5C6767
P 9050 4100
F 0 "#PWR0128" H 9050 3950 50  0001 C CNN
F 1 "+5V" H 9065 4273 50  0000 C CNN
F 2 "" H 9050 4100 50  0001 C CNN
F 3 "" H 9050 4100 50  0001 C CNN
	1    9050 4100
	1    0    0    -1  
$EndComp
Wire Wire Line
	9050 4100 9050 4150
Wire Wire Line
	9200 5250 9200 5300
Wire Wire Line
	9200 5500 9450 5500
Text GLabel 1450 3950 0    50   Input ~ 0
CAN_RXD
Wire Wire Line
	1450 3950 1850 3950
Text GLabel 1450 4050 0    50   Input ~ 0
CAN_TXD
Wire Wire Line
	1450 4050 1850 4050
Text Label 1700 2750 0    50   ~ 0
PA9
Wire Wire Line
	1450 3050 1850 3050
Wire Wire Line
	1850 2950 1550 2950
Wire Wire Line
	1550 2950 1550 3150
Text Label 1650 3050 0    50   ~ 0
PA10
Text Label 1650 3950 0    50   ~ 0
PB8
Text Label 1650 4050 0    50   ~ 0
PB9
Text GLabel 3150 3650 2    50   Input ~ 0
STATUS_LED
Wire Wire Line
	2750 3350 3150 3350
Text Label 2800 3350 0    50   ~ 0
PA3
Text GLabel 3150 3050 2    50   Input ~ 0
ERROR_LED
Text GLabel 3150 3850 2    50   Input ~ 0
CAN_STBY
Text GLabel 3150 3750 2    50   Input ~ 0
DRIVER_SF
Text GLabel 3150 3350 2    50   Input ~ 0
DRIVER_EN
Wire Wire Line
	1650 2300 1650 2850
Text GLabel 3150 2550 2    50   Input ~ 0
DRIVER_INV
Text GLabel 3150 3150 2    50   Input ~ 0
DRIVER_EN_SENSE
NoConn ~ 1850 2450
NoConn ~ 1850 2650
NoConn ~ 1850 3150
NoConn ~ 1850 3250
NoConn ~ 1850 3350
NoConn ~ 1850 3450
NoConn ~ 1850 3550
NoConn ~ 1850 3650
NoConn ~ 1850 3850
NoConn ~ 1850 4650
NoConn ~ 1850 4750
NoConn ~ 1850 4850
NoConn ~ 2750 2450
NoConn ~ 2750 2650
NoConn ~ 2750 2750
NoConn ~ 2750 3550
NoConn ~ 2750 3950
NoConn ~ 2750 4150
NoConn ~ 2750 4250
NoConn ~ 2750 4650
NoConn ~ 2750 4750
$Comp
L custom:Pololu_1185 U6
U 1 1 5E6EA20E
P 5500 2350
F 0 "U6" H 5500 2715 50  0000 C CNN
F 1 "Pololu_1185" H 5500 2624 50  0000 C CNN
F 2 "custom:Pololu_1185" H 5500 2350 50  0001 C CNN
F 3 "" H 5500 2350 50  0001 C CNN
	1    5500 2350
	1    0    0    -1  
$EndComp
$Comp
L power:+5V #PWR02
U 1 1 5E6EB6C8
P 5050 2150
F 0 "#PWR02" H 5050 2000 50  0001 C CNN
F 1 "+5V" H 5065 2323 50  0000 C CNN
F 2 "" H 5050 2150 50  0001 C CNN
F 3 "" H 5050 2150 50  0001 C CNN
	1    5050 2150
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR01
U 1 1 5E6EBB91
P 4950 2550
F 0 "#PWR01" H 4950 2300 50  0001 C CNN
F 1 "GND" H 4955 2377 50  0000 C CNN
F 2 "" H 4950 2550 50  0001 C CNN
F 3 "" H 4950 2550 50  0001 C CNN
	1    4950 2550
	1    0    0    -1  
$EndComp
Wire Wire Line
	5150 2350 5050 2350
Wire Wire Line
	5050 2350 5050 2150
Wire Wire Line
	5150 2250 4950 2250
Wire Wire Line
	4950 2250 4950 2550
$Comp
L power:+15V #PWR03
U 1 1 5E6F7D4C
P 5950 2200
F 0 "#PWR03" H 5950 2050 50  0001 C CNN
F 1 "+15V" H 5965 2373 50  0000 C CNN
F 2 "" H 5950 2200 50  0001 C CNN
F 3 "" H 5950 2200 50  0001 C CNN
	1    5950 2200
	1    0    0    -1  
$EndComp
Wire Wire Line
	5850 2300 5950 2300
Wire Wire Line
	5950 2300 5950 2200
Wire Wire Line
	6100 2950 6100 2400
Wire Wire Line
	6100 2400 5850 2400
Wire Wire Line
	6100 2400 7000 2400
Wire Wire Line
	7000 2400 7000 3350
Connection ~ 6100 2400
Text GLabel 4800 2450 0    50   Input ~ 0
CURRENT_SENSE
Wire Wire Line
	4800 2450 5150 2450
Text GLabel 3150 3250 2    50   Input ~ 0
CURRENT_SENSE
Text Label 1650 4250 0    50   ~ 0
PC0
Text Notes 6050 2300 0    50   ~ 0
This is not an error. We want the current to be negative\nso the output is between 0 and 2.5 V and so compatible\nwith ADC pins of the STM32 that only support 0-3.3V
Wire Wire Line
	1450 4250 1850 4250
Text GLabel 4600 4950 0    50   Input ~ 0
DRIVER_EN
Text GLabel 4600 4850 0    50   Input ~ 0
DRIVER_INV
$Comp
L power:GND #PWR04
U 1 1 5E75218B
P 4700 5100
F 0 "#PWR04" H 4700 4850 50  0001 C CNN
F 1 "GND" H 4705 4927 50  0000 C CNN
F 2 "" H 4700 5100 50  0001 C CNN
F 3 "" H 4700 5100 50  0001 C CNN
	1    4700 5100
	1    0    0    -1  
$EndComp
Wire Wire Line
	4600 4850 4800 4850
Wire Wire Line
	4600 4950 4800 4950
Wire Wire Line
	4700 5100 4700 5050
Wire Wire Line
	4700 5050 4800 5050
$Comp
L Connector:Screw_Terminal_01x03 J5
U 1 1 5E750A60
P 5000 4950
F 0 "J5" H 5000 5200 50  0000 L CNN
F 1 "DCC signal" H 5100 4950 50  0000 L CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x03_P2.54mm_Vertical" H 5000 4950 50  0001 C CNN
F 3 "~" H 5000 4950 50  0001 C CNN
	1    5000 4950
	1    0    0    -1  
$EndComp
Text GLabel 2250 5900 0    50   Input ~ 0
SPI_SCK
Text GLabel 2250 6000 0    50   Input ~ 0
SPI_MISO
Text GLabel 2250 6100 0    50   Input ~ 0
SPI_MOSI
Text GLabel 1450 4150 0    50   Input ~ 0
SPI_SCK
Wire Wire Line
	1450 4150 1850 4150
Text Label 1650 4150 0    50   ~ 0
PA5
Text GLabel 1450 3750 0    50   Input ~ 0
SPI_MISO
Wire Wire Line
	1450 3750 1850 3750
Text Label 1650 3750 0    50   ~ 0
PA6
Text GLabel 1450 4550 0    50   Input ~ 0
SPI_MOSI
Wire Wire Line
	1450 4550 1850 4550
Text Label 1650 4550 0    50   ~ 0
PA7
Wire Wire Line
	2750 3750 3150 3750
Text Label 2800 3750 0    50   ~ 0
PB10
Wire Wire Line
	2750 3850 3150 3850
Text Label 2800 3850 0    50   ~ 0
PB11
Text GLabel 1450 4350 0    50   Input ~ 0
EXT1
Text GLabel 1450 4250 0    50   Input ~ 0
EXT2
Text GLabel 1450 4950 0    50   Input ~ 0
EXT3
Text GLabel 2250 6200 0    50   Input ~ 0
EXT1
Text GLabel 2250 6300 0    50   Input ~ 0
EXT2
Text GLabel 2250 6400 0    50   Input ~ 0
EXT3
$Comp
L power:GND #PWR06
U 1 1 5E816636
P 2400 6550
F 0 "#PWR06" H 2400 6300 50  0001 C CNN
F 1 "GND" H 2405 6377 50  0000 C CNN
F 2 "" H 2400 6550 50  0001 C CNN
F 3 "" H 2400 6550 50  0001 C CNN
	1    2400 6550
	1    0    0    -1  
$EndComp
$Comp
L power:+5V #PWR05
U 1 1 5E816C64
P 2400 5750
F 0 "#PWR05" H 2400 5600 50  0001 C CNN
F 1 "+5V" H 2415 5923 50  0000 C CNN
F 2 "" H 2400 5750 50  0001 C CNN
F 3 "" H 2400 5750 50  0001 C CNN
	1    2400 5750
	1    0    0    -1  
$EndComp
$Comp
L Connector_Generic:Conn_01x08 J6
U 1 1 5E81DC6F
P 2650 6100
F 0 "J6" H 2730 6092 50  0000 L CNN
F 1 "Extension" H 2730 6001 50  0000 L CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x08_P2.54mm_Vertical" H 2650 6100 50  0001 C CNN
F 3 "~" H 2650 6100 50  0001 C CNN
	1    2650 6100
	1    0    0    -1  
$EndComp
Wire Wire Line
	2400 5800 2400 5750
Wire Wire Line
	2400 5800 2450 5800
Wire Wire Line
	2450 6500 2400 6500
Wire Wire Line
	2400 6500 2400 6550
Wire Wire Line
	2250 5900 2450 5900
Wire Wire Line
	2250 6000 2450 6000
Wire Wire Line
	2250 6100 2450 6100
Wire Wire Line
	2250 6200 2450 6200
Wire Wire Line
	2250 6300 2450 6300
Wire Wire Line
	2250 6400 2450 6400
NoConn ~ 2750 3450
$Comp
L Device:CP C3
U 1 1 5E5D643D
P 3700 1250
F 0 "C3" H 3818 1296 50  0000 L CNN
F 1 "10uF" H 3818 1205 50  0000 L CNN
F 2 "Capacitor_THT:CP_Radial_D5.0mm_P2.00mm" H 3738 1100 50  0001 C CNN
F 3 "~" H 3700 1250 50  0001 C CNN
	1    3700 1250
	1    0    0    -1  
$EndComp
Wire Wire Line
	3550 900  3700 900 
Wire Wire Line
	3700 900  3700 850 
Connection ~ 3700 900 
Wire Wire Line
	3700 900  4250 900 
Wire Wire Line
	3550 1000 4100 1000
Wire Wire Line
	4100 1000 4100 1200
$Comp
L power:GND #PWR07
U 1 1 5E5E6744
P 3700 1400
F 0 "#PWR07" H 3700 1150 50  0001 C CNN
F 1 "GND" H 3705 1227 50  0000 C CNN
F 2 "" H 3700 1400 50  0001 C CNN
F 3 "" H 3700 1400 50  0001 C CNN
	1    3700 1400
	1    0    0    -1  
$EndComp
Wire Wire Line
	3700 900  3700 1100
NoConn ~ 1850 2550
Wire Wire Line
	2750 2550 3150 2550
Text Label 2800 2550 0    50   ~ 0
PC2
NoConn ~ 1850 4450
Wire Wire Line
	2750 3150 3150 3150
Text Label 2800 3150 0    50   ~ 0
PA1
Wire Wire Line
	2750 3050 3150 3050
Text Label 2800 3050 0    50   ~ 0
PA2
NoConn ~ 2750 4050
NoConn ~ 2750 4550
Wire Wire Line
	1450 4950 1850 4950
Text Label 1650 4950 0    50   ~ 0
PB1
Wire Wire Line
	1450 4350 1850 4350
Text Label 1650 4350 0    50   ~ 0
PC1
NoConn ~ 2750 4350
NoConn ~ 2750 4450
Wire Wire Line
	4000 2950 4000 3000
Wire Wire Line
	2750 2950 4000 2950
Wire Wire Line
	2750 3650 3150 3650
Text Label 2800 3650 0    50   ~ 0
PC5
Wire Wire Line
	3150 3250 2750 3250
Text Label 2800 3250 0    50   ~ 0
PC3
$EndSCHEMATC
